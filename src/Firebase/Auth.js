import React from 'react';


import Login from '../pages/Login';
import Signup from '../pages/Signup';
import App from '../App';
import AppClave from '../AppClave';
import {
    BrowserRouter as ReactRouter,
    Link,
    Route
} from 'react-router-dom';
import Dashboard from '../pages/dashboard/Dashboard';
import EncuestaB from '../components/encuestas/EncuestaB';
import EncuestaA from '../components/encuestas/EncuestaA';
import SeccionB1 from '../components/encuestas/EncuestasB/SeccionB1';
import SeccionB2 from '../components/encuestas/EncuestasB/SeccionB2';
import SeccionB3 from '../components/encuestas/EncuestasB/SeccionB3';
import SeccionB4 from '../components/encuestas/EncuestasB/SeccionB4';
import SeccionB5 from '../components/encuestas/EncuestasB/SeccionB5';
import SeccionB6 from '../components/encuestas/EncuestasB/SeccionB6';
import SeccionB7 from '../components/encuestas/EncuestasB/SeccionB7';
import SeccionB8 from '../components/encuestas/EncuestasB/SeccionB8';
import SeccionB9 from '../components/encuestas/EncuestasB/SeccionB9';
import SeccionB10 from '../components/encuestas/EncuestasB/SeccionB10';
import SeccionB11 from '../components/encuestas/EncuestasB/SeccionB11';
import SeccionB12 from '../components/encuestas/EncuestasB/SeccionB12';
import SeccionB13 from '../components/encuestas/EncuestasB/SeccionB13';
import SeccionB14 from '../components/encuestas/EncuestasB/SeccionB14';
import SeccionB15 from '../components/encuestas/EncuestasB/SeccionB15';
import SeccionB16 from '../components/encuestas/EncuestasB/SeccionB16';
import Clave from '../pages/Clave';

import Admin from '../pages/Admin';
import firebase from '../Firebase/Firebase';

export default function singInRouter(valor) {
        

    if (valor) {

        return (
            <ReactRouter>
                <App>

                    <Route exact path="/" component={Login}></Route>
                    <Route path="/login" component={Login}></Route>
                    <Route path="/signup" component={Signup}></Route>

                    <Route path="/EncuestaB" component={EncuestaB}></Route>
                    <Route path="/EncuestaA" component={EncuestaA}></Route>
                    <Route path="/Resultados" component={Dashboard}></Route>
                    <Route path="/Clave" component={Clave}></Route>
                    <Route path="/Admin" component={Admin}></Route>
                    <Route path="/SeccionB1" component={SeccionB1}></Route>
                    <Route path="/SeccionB2" component={SeccionB2}></Route>
                    <Route path="/SeccionB3" component={SeccionB3}></Route>
                    <Route path="/SeccionB4" component={SeccionB4}></Route>
                    <Route path="/SeccionB5" component={SeccionB5}></Route>
                    <Route path="/SeccionB6" component={SeccionB6}></Route>
                    <Route path="/SeccionB7" component={SeccionB7}></Route>
                    <Route path="/SeccionB8" component={SeccionB8}></Route>
                    <Route path="/SeccionB9" component={SeccionB9}></Route>
                    <Route path="/SeccionB10" component={SeccionB10}></Route>
                    <Route path="/SeccionB11" component={SeccionB11}></Route>
                    <Route path="/SeccionB12" component={SeccionB12}></Route>
                    <Route path="/SeccionB13" component={SeccionB13}></Route>
                    <Route path="/SeccionB14" component={SeccionB14}></Route>
                    <Route path="/SeccionB15" component={SeccionB15}></Route>
                    <Route path="/SeccionB16" component={SeccionB16}></Route>
                </App>

            </ReactRouter>
        )
    } else {
        return (
            <ReactRouter>
                <AppClave>
                    <Route path="/Clave" component={Clave}></Route>
                    <Route exact path="/" component={Login}></Route>
                    <Route path="/Resultados" component={Login}></Route>
                </AppClave>

            </ReactRouter>
        )
    }

}