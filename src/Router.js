import React from 'react';
import Login from './pages/Login';
import App from './App';
import AppClave from './AppClave';
import {
    BrowserRouter as ReactRouter,
    Route
} from 'react-router-dom';
import Clave from './pages/Clave';
import TablaGeneral from './pages/TablaGeneral';
import TablaDepa from './pages/TablaDepa';
import TablaClave from './pages/TablaClave';
import Admin from './pages/Admin';
import MenuGrafica from './pages/MenuGrafica';
import Gracias from './pages/Gracias';

//const usersingIn = true
const usersingIn = false
export default class Router extends React.Component {
    //Rutas solo para personas logeadas
    singInRouter() {
        if (usersingIn) {

            return (
                <ReactRouter>
                    <App>
                        <Route exact path="/" component={Login}></Route>
                        <Route path="/login" component={Login}></Route>
                        <Route path="/Clave" component={Clave}></Route>
                        <Route path="/Admin" component={Admin}></Route>

                    </App>

                </ReactRouter>
            )
        } else {
             //rutas para cualquier persona
            return (
                <ReactRouter>
                    <AppClave>
                    <Route path="/Admin" component={Admin}></Route>
                        <Route path="/Clave" component={Clave}></Route>
                        <Route exact path="/" component={MenuGrafica}></Route>
                        <Route path="/General" component={TablaGeneral}></Route>
                        <Route path="/Departamento" component={TablaDepa}></Route>
                        <Route path="/Folio" component={TablaClave}></Route>
                        <Route path="/login" component={Login}></Route>
                        <Route path="/Gracias" component={Gracias}></Route>
                    </AppClave>
                </ReactRouter>
            )
        }

    }
    render() {
        return (
           
                <ReactRouter>
                    {this.singInRouter()}
                </ReactRouter>
        );
    }
}
