import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBarMenu from './components/navigation/AppBarMenu';
import  './App.css';


class App extends Component {
  constructor(props){
    super(props);
  }
  render() {
    return (
      <MuiThemeProvider>
        <AppBarMenu/>
        {this.props.children}
      </MuiThemeProvider>
    );
  }
}
export default App;
