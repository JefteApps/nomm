import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Rambiente, Rdominio, Rarray, Rreferencias } from '../pages/Resultados';
import firebase from '../Firebase/Firebase';
var aux = 1
const nameRefDepaNew = ""
var resp = []
var sumaCat1 = 0
var 
  di1 = 0,
  di2 = 0,
  di3 = 0,
  di4 = 0,
  di5 = 0,
  di6 = 0,
  di7 = 0,
  di8 = 0,
  di9 = 0,
  di10 = 0,
  di11 = 0,
  di12 = 0,
  di13 = 0,
  di14 = 0,
  di15 = 0,
  di16 = 0,
  di17 = 0,
  di18 = 0,
  di19 = 0,
  di20 = 0,
  di21 = 0,
  di22 = 0,
  di23 = 0,
  di24 = 0,
  di25 = 0,
  di1d = 0,
  di2d = 0,
  di3d = 0,
  di4d = 0,
  di5d = 0,
  di6d = 0,
  di7d = 0,
  di8d = 0,
  di9d = 0,
  di10d = 0,
  di11d = 0,
  di12d = 0,
  di13d = 0,
  di14d = 0,
  di15d = 0,
  di16d = 0,
  di17d = 0,
  di18d = 0,
  di19d = 0,
  di20d = 0,
  di21d = 0,
  di22d = 0,
  di23d = 0,
  di24d = 0,
  di25d = 0,
  di1g = 0,
  di2g = 0,
  di3g = 0,
  di4g = 0,
  di5g = 0,
  di6g = 0,
  di7g = 0,
  di8g = 0,
  di9g = 0,
  di10g = 0,
  di11g = 0,
  di12g = 0,
  di13g = 0,
  di14g = 0,
  di15g = 0,
  di16g = 0,
  di17g = 0,
  di18g = 0,
  di19g = 0,
  di20g = 0,
  di21g = 0,
  di22g = 0,
  di23g = 0,
  di24g = 0,
  di25g = 0
var contGlobal = 0, contDepa = 0
//Variables de dominio por clave
var condiciones = 0,
  carga = 0,
  falta = 0,
  jornada = 0,
  inter = 0,
  lider = 0,
  relaciones = 0,
  violencia = 0,
  reconocimiento = 0,
  insuficiente = 0,
  //variables dominio por departamento
  condicionesd = 0,
  cargad = 0,
  faltad = 0,
  jornadad = 0,
  interd = 0,
  liderd = 0,
  relacionesd = 0,
  violenciad = 0,
  reconocimientod = 0,
  insuficiented = 0,
  //Variables dominio globales
  condicionesg = 0,
  cargag = 0,
  faltag = 0,
  jornadag = 0,
  interg = 0,
  liderg = 0,
  relacionesg = 0,
  violenciag = 0,
  reconocimientog = 0,
  insuficienteg = 0
//Variables de categoria
var ambienteA = 0,
  ambiente = 0,
  factores = 0,
  org = 0,
  liderCat = 0,
  entorno = 0,
  //variables categoria por departamento
  ambiented = 0,
  factoresd = 0,
  orgd = 0,
  liderCatd = 0,
  entornod = 0,
  //Variables categoria globales
  ambienteg = 0,
  factoresg = 0,
  orgg = 0,
  liderCatg = 0,
  entornog = 0
var finalEncuesta = 0

/**TODAS LAS RESPUESTAS EN UN ARRAY */
export function Sumar(index, valor) {
  resp[index] = valor
}


/**DOMINIO*/
export function Dominio() {
  /** Ambiente */
  //Condiciones
  di1 = resp[0] + resp[2]
  di2 = resp[1] + resp[3]
  di3 = resp[4]

  di4 = resp[5] + resp[11]
  di5 = resp[6] + resp[7]
  di6 = resp[8] + resp[9] + resp[10]
  di7 = resp[64] + resp[65] + resp[66] + resp[67]
  di8 = resp[12] + resp[13]
  di9 = resp[14] + resp[15]

  di10 = resp[24] + resp[25] + resp[26] + resp[27]
  di11 = resp[22] + resp[23]
  di12 = resp[28] + resp[29]
  di13 = resp[34] + resp[35]

  di14 = resp[16] + resp[17]

  di15 = resp[18] + resp[19]
  di16 = resp[20] + resp[21]

  di17 = resp[30] + resp[31] + resp[32] + resp[33]
  di18 = resp[36] + resp[37] + resp[38] + resp[39] + resp[40]

  di19 = resp[41] + resp[42] + resp[43] + resp[44] + resp[45]
  di20 = resp[68] + resp[69] + resp[70] + resp[71]

  di21 = resp[56] + resp[57] + resp[58] + resp[59] + resp[60] + resp[61] + resp[62] + resp[63]

  di22 = resp[46] + resp[47]
  di23 = resp[48] + resp[49] + resp[50] + resp[51]

  di24 = resp[54] + resp[55]
  di25 = resp[52] + resp[53]
  //Condiciones
  condiciones = di1 + di2 + di3
  //Carga
  carga = di4 + di5 + di6 + di7 + di8 + di9
  //falta
  falta = di10 + di11 + di12 + di13
  //jornada
  jornada = di14
  //inter
  inter = di15 + di16
  //lider
  lider = di17 + di18
  //relaciones
  relaciones = di19 + di20
  //violencia
  violencia = di21
  //reconocimiento
  reconocimiento = di22 + di23
  //insuficiente
  insuficiente = di24 + di25
  //Sumar categorias
  ambiente = condiciones
  factores = carga + falta
  org = jornada + inter
  liderCat = lider + relaciones + violencia
  entorno = reconocimiento + insuficiente
  //sumatoria final
  finalEncuesta = ambiente + factores + org + liderCat + entorno
  /*for(var i=0;i<=72;i++){
    alert(i+" resultado: "+resp[i])
  }*/
}
function subirFireBase(empresa, encuesta, clave, depa) {
  //Subir por clave
  firebase.database().ref('empresa/' + empresa + '/' + encuesta + '/' + 'clave/' + clave).update({
    //Categoria Ambiente
    ambiente: ambiente + "",
    //Dominio
    condiciones: condiciones + "",
    //Categoria Factores
    factores: factores + "",
    //Dominio
    carga: carga + "",
    falta: falta + "",
    //Categoria Org
    org: org + "",
    //Dominio
    jornada: jornada + "",
    inter: inter + "",
    //Categoria Liderazgo
    liderCat: liderCat + "",
    //Dominio
    lider: lider + "",
    relaciones: relaciones + "",
    violencia: violencia + "",
    //Categoria Entorno
    entorno: entorno + "",
    //Dominio
    reconocimiento: reconocimiento + "",
    insuficiente: insuficiente + "",
    di1: di1 + "",
    di2: di2 + "",
    di3: di3 + "",
    di4: di4 + "",
    di5: di5 + "",
    di6: di6 + "",
    di7: di7 + "",
    di8: di8 + "",
    di9: di9 + "",
    di10: di10 + "",
    di11: di11 + "",
    di12: di12 + "",
    di13: di13 + "",
    di14: di14 + "",
    di15: di15 + "",
    di16: di16 + "",
    di17: di17 + "",
    di18: di18 + "",
    di19: di19 + "",
    di20: di20 + "",
    di21: di21 + "",
    di22: di22 + "",
    di23: di23 + "",
    di24: di24 + "",
    di25: di25 + "",
    valor: 2

  });

  //Subir contador Departamento
  const nameRefDepa = firebase.database().ref()
    .child('empresa')
    .child('' + empresa)
    .child('' + encuesta)
    .child('depa')
  //bajar contador de Departamento
  nameRefDepa.on('value', (snapshot) => {
    if (snapshot.val().cont) {
      contDepa = 0
    } else {
      contDepa = snapshot.val().cont
    }
  })
  //sumar contador de Departamento
  contDepa++
  //subir contador de Departamento
  nameRefDepa.update({
    cont: contDepa
  })

  nameRefDepa.child('' + depa).once('value', (snapshot) => {
    //alert("aqui baja valores depa")
    contDepa = snapshot.val().cont + 1
    //subirNEW(snapshot.val().ambiente,ambiente,"ambiente",nameRefDepa,depa)
    ambiented = snapshot.val().ambiente + ambiente
    //Dominio
    condicionesd = (snapshot.val().condiciones) + condiciones
    //Categoria Factores
    factoresd = (snapshot.val().factores) + factores
    //Dominio
    cargad = (snapshot.val().carga) + carga
    faltad = (snapshot.val().falta) + falta
    //Categoria Org
    orgd = (snapshot.val().org) + org
    //Dominio
    jornadad = (snapshot.val().jornada) + jornada
    interd = (snapshot.val().inter) + inter
    //Categoria Liderazgo
    liderCatd = (snapshot.val().liderCat) + liderCat
    //Dominio
    liderd = (snapshot.val().lider) + lider
    relacionesd = (snapshot.val().relaciones) + relaciones
    violenciad = (snapshot.val().violencia) + violencia
    //Categoria Entorno
    entornod = (snapshot.val().entorno) + entorno
    //Dominio
    reconocimientod = (snapshot.val().reconocimiento) + reconocimiento
    insuficiented = (snapshot.val().insuficiente) + insuficiente
    di1d = (snapshot.val().di1) + di1
    di2d = (snapshot.val().di2) + di2
    di3d = (snapshot.val().di3) + di3
    di4d = (snapshot.val().di4) + di4
    di5d = (snapshot.val().di5) + di5
    di6d = (snapshot.val().di6) + di6
    di7d = (snapshot.val().di7) + di7
    di8d = (snapshot.val().di8) + di8
    di9d = (snapshot.val().di9) + di9
    di10d = (snapshot.val().di10) + di10
    di11d = (snapshot.val().di11) + di11
    di12d = (snapshot.val().di12) + di12
    di13d = (snapshot.val().di13) + di13
    di14d = (snapshot.val().di14) + di14
    di15d = (snapshot.val().di15) + di15
    di16d = (snapshot.val().di16) + di16
    di17d = (snapshot.val().di17) + di17
    di18d = (snapshot.val().di18) + di18
    di19d = (snapshot.val().di19) + di19
    di20d = (snapshot.val().di20) + di20
    di21d = (snapshot.val().di21) + di21
    di22d = (snapshot.val().di22) + di22
    di23d = (snapshot.val().di23) + di23
    di24d = (snapshot.val().di24) + di24
    di25d = (snapshot.val().di25) + di25
    //ambienteA = ambienteA + ambiente

    nameRefDepa.child('' + depa).update({
      //Categoria Ambiente
      cont: contDepa,
      ambiente: ambiented,
      //Dominio
      condiciones: condicionesd,
      //Categoria Factores
      factores: factoresd,
      //Dominio
      carga: cargad,
      falta: faltad,
      //Categoria Org
      org: orgd,
      //Dominio
      jornada: jornadad,
      inter: interd,
      //Categoria Liderazgo
      liderCat: liderCatd,
      //Dominio
      lider: liderd,
      relaciones: relacionesd,
      violencia: violenciad,
      //Categoria Entorno
      entorno: entornod,
      //Dominio
      reconocimiento: reconocimientod,
      insuficiente: insuficiented,
      di1: di1d,
      di2: di2d,
      di3: di3d,
      di4: di4d,
      di5: di5d,
      di6: di6d,
      di7: di7d,
      di8: di8d,
      di9: di9d,
      di10: di10d,
      di11: di11d,
      di12: di12d,
      di13: di13d,
      di14: di14d,
      di15: di15d,
      di16: di16d,
      di17: di17d,
      di18: di18d,
      di19: di19d,
      di20: di20d,
      di21: di21d,
      di22: di22d,
      di23: di23d,
      di24: di24d,
      di25: di25d
    })


    //alert("subio ambiente a depa: " + snapshot.val().ambiente + "  " + ambiente + "Ambiente aux " + ambienteA)

  })


  var nameRefDepaNew = firebase.database().ref()
    .child('empresa')
    .child('' + empresa)
    .child('' + encuesta)
    .child('depa').child('' + depa)
  // subirDepa(nameRefDepa,depa)

  //bajar todos los valores

  //Subir contador GLOBAL
  const nameRefGeneral = firebase.database().ref()
    .child('empresa')
    .child('' + empresa)
    .child('' + encuesta)
  nameRefGeneral.child('general').once('value', (snapshot) => {
    contGlobal = snapshot.val().cont + 1
    nameRefGeneral.update({
      cont: contGlobal
    })
  })
  nameRefGeneral.child('general').once('value', (snapshot) => {
    contGlobal = snapshot.val().cont + 1
    ambienteg = snapshot.val().ambiente + ambiente
    //Dominio
    condicionesg = (snapshot.val().condiciones) + condiciones
    //Categoria Factores
    factoresg = (snapshot.val().factores) + factores
    //Dominio
    cargag = (snapshot.val().carga) + carga
    faltag = (snapshot.val().falta) + falta
    //Categoria Org
    orgg = (snapshot.val().org) + org
    //Dominio
    jornadag = (snapshot.val().jornada) + jornada
    interg = (snapshot.val().inter) + inter
    //Categoria Liderazgo
    liderCatg = (snapshot.val().liderCat) + liderCat
    //Dominio
    liderg = (snapshot.val().lider) + lider
    relacionesg = (snapshot.val().relaciones) + relaciones
    violenciag = (snapshot.val().violencia) + violencia
    //Categoria Entorno
    entornog = (snapshot.val().entorno) + entorno
    //Dominio
    reconocimientog = (snapshot.val().reconocimiento) + reconocimiento
    insuficienteg = (snapshot.val().insuficiente) + insuficiente
    di1g = (snapshot.val().di1) + di1
    di2g = (snapshot.val().di2) + di2
    di3g = (snapshot.val().di3) + di3
    di4g = (snapshot.val().di4) + di4
    di5g = (snapshot.val().di5) + di5
    di6g = (snapshot.val().di6) + di6
    di7g = (snapshot.val().di7) + di7
    di8g = (snapshot.val().di8) + di8
    di9g = (snapshot.val().di9) + di9
    di10g = (snapshot.val().di10) + di10
    di11g = (snapshot.val().di11) + di11
    di12g = (snapshot.val().di12) + di12
    di13g = (snapshot.val().di13) + di13
    di14g = (snapshot.val().di14) + di14
    di15g = (snapshot.val().di15) + di15
    di16g = (snapshot.val().di16) + di16
    di17g = (snapshot.val().di17) + di17
    di18g = (snapshot.val().di18) + di18
    di19g = (snapshot.val().di19) + di19
    di20g = (snapshot.val().di20) + di20
    di21g = (snapshot.val().di21) + di21
    di22g = (snapshot.val().di22) + di22
    di23g = (snapshot.val().di23) + di23
    di24g = (snapshot.val().di24) + di24
    di25g = (snapshot.val().di25) + di25
    nameRefGeneral.child('general').update({
      cont: contGlobal,
      ambiente: ambienteg,
      //Dominio
      condiciones: condicionesg,
      //Categoria Factores
      factores: factoresg,
      //Dominio
      carga: cargag,
      falta: faltag,
      //Categoria Org
      org: orgg,
      //Dominio
      jornada: jornadag,
      inter: interg,
      //Categoria Liderazgo
      liderCat: liderCatg,
      //Dominio
      lider: liderg,
      relaciones: relacionesg,
      violencia: violenciag,
      //Categoria Entorno
      entorno: entornog,
      //Dominio
      reconocimiento: reconocimientog,
      insuficiente: insuficienteg,
      di1: di1g,
      di2: di2g,
      di3: di3g,
      di4: di4g,
      di5: di5g,
      di6: di6g,
      di7: di7g,
      di8: di8g,
      di9: di9g,
      di10: di10g,
      di11: di11g,
      di12: di12g,
      di13: di13g,
      di14: di14g,
      di15: di15g,
      di16: di16g,
      di17: di17g,
      di18: di18g,
      di19: di19g,
      di20: di20g,
      di21: di21g,
      di22: di22g,
      di23: di23g,
      di24: di24g,
      di25: di25g

    })
  })


  //alert("El contador globarl es " + contGlobal)
}



export function FinalizarSuma() {
  for (var i = 0; i <= 8; i++) {
    sumaCat1 = sumaCat1 + resp[i]
  }
  //alert(resp[8])
  sumaCat1 = 0

}


export function condicionCategoria() {
  return (Rambiente(ambiente, factores, org, liderCat, entorno))
}
export function condiDominio() {
  return (Rdominio(condiciones, carga, falta, jornada, inter, lider, relaciones, violencia, reconocimiento, insuficiente))
}

export function condiArray() {
  return (Rarray(resp))
}


const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
});
export function Linear() {
  const classes = useStyles();
  const [completed, setCompleted] = React.useState(0);

  React.useEffect(() => {
    function progress() {
      setCompleted(oldCompleted => {
        if (oldCompleted === 100) {
          return 100;
        }
        //const diff = Math.random() * 10;
        const diff = 10;
        return Math.min(oldCompleted + diff, 100);
      });
    }

    const timer = setInterval(progress, 500);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className={classes.root}>

    </div>
  );
}


export function SumaValores(idEmpresa, idEcuesta, idClave, idDepa) {
  //alert(idEmpresa + idEcuesta + idClave)
  subirFireBase(idEmpresa, idEcuesta, idClave, idDepa)
  Rreferencias(idEmpresa, idEcuesta, idClave, idDepa)
  //subirDepa(idEmpresa, idEcuesta, idClave, idDepa)
}
