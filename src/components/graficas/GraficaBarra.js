import React from 'react';
import GaugeChart from 'react-gauge-chart';
import { indigo400, redA700, orange500, yellow500, lightGreenA400, cyanA400 } from 'material-ui/styles/colors';
import firebase from '../../Firebase/Firebase';


var contI = 1, contGlobal = 0
export class GraficaBarra extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vAmbiente: 0,
            vFactores: 0,
            vOrg: 0,
            vLiderCat: 0,
            vEntorno: 0,
            vContEnc: 0,
            vResultado: 0
        }
    }

    componentWillMount() {
        try {
            var nameRefX
            // alert("tipo "+idMZ)
            switch (this.props.idLugar) {
                //General
                case "1": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('general')
                    nameRefX.child('cont').on('value', (snapshot) => {
                        contI = snapshot.val()
                    })
                    break
                //Departamento
                case "2": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('depa')
                    .child('' + this.props.idDepa)
                    nameRefX.child('cont').on('value', (snapshot) => {
                        contI = snapshot.val()
                    })
                    break
                //Folio/Clave
                case "3": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('clave')
                    .child('' + this.props.idClave)
                    contI = 1
                    break

            }
            //alert("mi ruta es "+nameRefX)
            nameRefX.on('value', (snapshot) => {
                this.setState({
                    vAmbiente: soloDecimal(snapshot.val().ambiente / contI),
                    vFactores: soloDecimal(snapshot.val().factores / contI),
                    vOrg: soloDecimal(snapshot.val().org / contI),
                    vLiderCat: soloDecimal(snapshot.val().liderCat / contI),
                    vEntorno: soloDecimal(snapshot.val().entorno / contI)
                })
                var s1 = this.state.vAmbiente
                var s2 = this.state.vFactores
                var s3 = this.state.vOrg
                var s4 = this.state.vLiderCat
                var s5 = this.state.vEntorno

                contGlobal = (((s1 + s2 + s3 + s4 + s5) * 100) / 288) / 100
                this.setState({
                    vResultado: contGlobal
                })
                //alert("valor d1 "+snapshot.val().di1+contI)
                //alert("El contador global es: "+this.state.vResultado)
            })

            const nameRefGeneral = firebase.database().ref()
                .child('empresa')
                .child('' + this.props.idEmpresa)
                .child('' + this.props.idEncuesta)


            nameRefGeneral.child('cont').on('value', (snapshot) => {
                contGlobal = snapshot.val().cont
            })
        }
        catch (error) {
            console.error(error);
            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    }
    render() {
        return (
            <GaugeChart id="gauge-chart5"
                nrOfLevels={280}
                arcsLength={[0.173611, 0.086799, 0.08334, 0.14236, 0.51389]}
                colors={[cyanA400, lightGreenA400, yellow500, orange500, redA700]}
                percent={this.state.vResultado}
                arcPadding={0.02}
                textColor="black"
            />
        );
    }
}


var cantidadEnc=0,auxCont=0
export class GraficaEncuestas extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vCantEncuestas: 0,
            vAux:0
        }
    }

    componentWillMount() {
        try {
            var nameRefX
            // alert("tipo "+idMZ)
            switch (this.props.idLugar) {
                //General
                case "1": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    nameRefX.child('general').child('cont').on('value', (snapshot) => {
                        contI = snapshot.val()
                    })
                    break

            }
            //alert("mi ruta es "+nameRefX)
            nameRefX.on('value', (snapshot) => {
                this.setState({
                    vCantEncuestas: snapshot.val().cant
                })
                var s1 = this.state.vCantEncuestas


                auxCont = ((contI*100)/s1)/100
                this.setState({
                    vAux: auxCont
                })
                //alert("valor d1 "+snapshot.val().di1+contI)
                //alert("El contador global es: " + this.state.vResultado)
            })

            const nameRefGeneral = firebase.database().ref()
                .child('empresa')
                .child('' + this.props.idEmpresa)
                .child('' + this.props.idEncuesta)


            nameRefGeneral.child('cont').on('value', (snapshot) => {
                contGlobal = snapshot.val().cont
            })
        }
        catch (error) {
            console.error(error);
            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    }
    render() {
        return (
            <div>

                <GaugeChart id="gauge-chart2"
                    nrOfLevels={20}
                    percent={this.state.vAux}
                    colors={[redA700, indigo400]}
                    textColor="black"
                />
            </div>
        );
    }
}
function soloDecimal(valorInicial) {
    var parteEntera, resultado;
    resultado = valorInicial;
    parteEntera = Math.floor(resultado);
    resultado = (resultado - parteEntera) * Math.pow(10, 1);
    resultado = Math.round(resultado);
    resultado = (resultado / Math.pow(10, 1)) + parteEntera;
    return resultado;
}

