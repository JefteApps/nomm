
import React from 'react';
import Chart from 'react-google-charts';
import { lighten, makeStyles, withStyles } from '@material-ui/core/styles';

import LinearProgress from '@material-ui/core/LinearProgress';
import {indigo400,redA700,orange500,yellow500,lightGreenA400,cyanA400} from 'material-ui/styles/colors';






const ProgressNulo = withStyles({
  root: {
    height: 25,
    backgroundColor: lighten(cyanA400, 0.5),
  },
  bar: {
    borderRadius: 0,
    backgroundColor: cyanA400,
  },
})(LinearProgress);

const ProgressBajo = withStyles({
  root: {
    height: 25,
    backgroundColor: lighten(lightGreenA400, 0.5),
  },
  bar: {
    borderRadius: 0,
    backgroundColor: lightGreenA400,
  },
})(LinearProgress);

const ProgressMedio = withStyles({
  root: {
    height: 25,
    backgroundColor: lighten(yellow500, 0.5),
  },
  bar: {
    borderRadius: 0,
    backgroundColor: yellow500,
  },
})(LinearProgress);

const ProgressAlto = withStyles({
  root: {
    height: 25,
    backgroundColor: lighten(orange500, 0.5),
  },
  bar: {
    borderRadius: 0,
    backgroundColor: orange500,
  },
})(LinearProgress);

const ProgressMuyAlto = withStyles({
  root: {
    height: 25,
    backgroundColor: lighten(redA700, 0.5),
  },
  bar: {
    borderRadius: 0,
    backgroundColor: redA700,
  },
})(LinearProgress);





const useStyles = makeStyles({
    root: {
      flexGrow: 1,
    },
  });
  export function LinearNulo() {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);
  
    React.useEffect(() => {
      function progress() {
        setCompleted(oldCompleted => {
          if (oldCompleted === 100) {
            return 100;
          }
          //const diff = Math.random() * 10;
          const diff = 10;
          return Math.min(oldCompleted + diff, 100);
        });
      }
  
      const timer = setInterval(progress, 500);
      return () => {
        clearInterval(timer);
      };
    }, []);
  
    return (
      <div className={classes.root}>
        <ProgressNulo
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={completed}
        style={{'marginRight':'80%'}}
      />
        <br />
      </div>
    );
  }
  
  export function LinearBajo() {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);
  
    React.useEffect(() => {
      function progress() {
        setCompleted(oldCompleted => {
          if (oldCompleted === 100) {
            return 100;
          }
          //const diff = Math.random() * 10;
          const diff = 10;
          return Math.min(oldCompleted + diff, 100);
        });
      }
  
      const timer = setInterval(progress, 500);
      return () => {
        clearInterval(timer);
      };
    }, []);
  
    return (
      <div className={classes.root}>
         <ProgressBajo
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={completed}
        style={{'marginRight':'60%'}}
      />
      </div>
    );
  }
  export function LinearMedio() {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);
  
    React.useEffect(() => {
      function progress() {
        setCompleted(oldCompleted => {
          if (oldCompleted === 100) {
            return 100;
          }
          //const diff = Math.random() * 10;
          const diff = 10;
          return Math.min(oldCompleted + diff, 100);
        });
      }
  
      const timer = setInterval(progress, 500);
      return () => {
        clearInterval(timer);
      };
    }, []);
  
    return (
      <div className={classes.root}>
         <ProgressMedio
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={completed}
        style={{'marginRight':'40%'}}
      />
      </div>
    );
  }
  export function LinearAlto() {
    const classes = useStyles();
    const [completed, setCompleted] = React.useState(0);
  
    React.useEffect(() => {
      function progress() {
        setCompleted(oldCompleted => {
          if (oldCompleted === 100) {
            return 100;
          }
          //const diff = Math.random() * 10;
          const diff = 10;
          return Math.min(oldCompleted + diff, 100);
        });
      }
  
      const timer = setInterval(progress, 500);
      return () => {
        clearInterval(timer);
      };
    }, []);
  
    return (
      <div className={classes.root}>
         <ProgressAlto
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={completed}
        style={{'marginRight':'20%'}}
      />
      </div>
    );
  }  
export function LinearMuyAlto() {
  const classes = useStyles();
  const [completed, setCompleted] = React.useState(0);

  React.useEffect(() => {
    function progress() {
      setCompleted(oldCompleted => {
        if (oldCompleted === 100) {
          return 100;
        }
        //const diff = Math.random() * 10;
        const diff = 10;
        return Math.min(oldCompleted + diff, 100);
      });
    }

    const timer = setInterval(progress, 500);
    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <div className={classes.root}>
      <ProgressMuyAlto
        className={classes.margin}
        variant="determinate"
        color="secondary"
        value={completed}
        style={{'marginRight':'0%'}}
      />
    </div>
  );
}