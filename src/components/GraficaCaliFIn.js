
import React from 'react';
import { redA700, orange500, yellow500, lightGreenA400, cyanA400 } from 'material-ui/styles/colors';
import Fab from '@material-ui/core/Fab';
import IconNulo from '@material-ui/icons/SentimentVerySatisfied';
import IconBajo from '@material-ui/icons/SentimentSatisfiedAlt';
import IconMedio from '@material-ui/icons/SentimentSatisfied';
import IconAlto from '@material-ui/icons/SentimentDissatisfied';
import IconMuyAlto from '@material-ui/icons/SentimentVeryDissatisfied';
import firebase from '../Firebase/Firebase';

var idMZ = 0


var contGlobal = ""
var sumatoriaFinal = 0
var contI = 1
var cantidadGlobal = ""

export default class GraficaCaliFin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vAmbiente: 0,
            vFactores: 0,
            vOrg: 0,
            vLiderCat: 0,
            vEntorno: 0,
            vSumatoria: 0
        }
    }

    componentWillMount() {
        try {
            var nameRefX
            idMZ = this.props.idLugar
            // alert("tipo "+idMZ)
            switch (idMZ) {
                //General
                case "1": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('general')
                    nameRefX.child('cont').on('value', (snapshot) => {
                        contI = snapshot.val()
                    })
                    break
                //Departamento
                case "2": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('depa')
                    .child('' + this.props.idDepa)
                    nameRefX.child('cont').on('value', (snapshot) => {
                        contI = snapshot.val()
                    })
                    break
                //Folio/Clave
                case "3": nameRefX = firebase.database().ref()
                    .child('empresa')
                    .child('' + this.props.idEmpresa)
                    .child('' + this.props.idEncuesta)
                    .child('clave')
                    .child('' + this.props.idClave)
                    contI = 1
                    break

            }
            //alert("mi ruta es "+nameRefX)
            nameRefX.on('value', (snapshot) => {
                this.setState({
                    vAmbiente: soloDecimal(snapshot.val().ambiente / contI),
                    vFactores: soloDecimal(snapshot.val().factores / contI),
                    vOrg: soloDecimal(snapshot.val().org / contI),
                    vLiderCat: soloDecimal(snapshot.val().liderCat / contI),
                    vEntorno: soloDecimal(snapshot.val().entorno / contI)
                })
                var s1 = this.state.vAmbiente
                var s2 = this.state.vFactores
                var s3 = this.state.vOrg
                var s4 = this.state.vLiderCat
                var s5 = this.state.vEntorno

                sumatoriaFinal = (s1 + s2 + s3 + s4 + s5)
                sumatoriaFinal=soloDecimal(sumatoriaFinal)
                //alert(s1 + "-"+s2 + "-"+s3 + "-"+s4 +"-"+ s5)
                this.setState({
                    vSumatoria: sumatoriaFinal
                })

                //alert("valor d1 "+snapshot.val().di1+contI)

            })
            const nameRefGeneral = firebase.database().ref()
                .child('empresa')
                .child('' + this.props.idEmpresa)
                .child('' + this.props.idEncuesta)


            nameRefGeneral.child('cont').on('value', (snapshot) => {
                contGlobal = snapshot.val().cont
            })
        }
        catch (error) {
            console.error(error);
            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    }


    render() {
        return (

            <div style={{'marginRight':'10px','marginBottom':'10px' , 'marginLeft':'10px'}}>
                <h4>Calificación final</h4>
                <h2 style={{'textAlign':'center'}}>{this.state.vSumatoria}</h2>
                <h4>Riesgo:</h4>
                {condiciones(this.state.vSumatoria)}
            </div>
        );
    }
}
function soloDecimal(valorInicial) {
    var parteEntera, resultado;
    resultado = valorInicial;
    parteEntera = Math.floor(resultado);
    resultado = (resultado - parteEntera) * Math.pow(10, 1);
    resultado = Math.round(resultado);
    resultado = (resultado / Math.pow(10, 1)) + parteEntera;
    return resultado;
}

function casosA(valorA, ambA, colorA, lvl) {
    switch (0) {
        case 0:
            return (
                //Primer boton
                <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorA,'marginRight':'10px','marginBottom':'10px' , 'marginLeft':'10px'}}>
                    {valorA}{iconCara(lvl)}
                </Fab>)
            break;
        case 1:
            //Segundo boton con carita
            return (
                <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorA }}>
                    {iconCara(lvl)}</Fab>)
            break;
        case 2:
            //Raya de color en el texto 

            return (<hr color={colorPromedio(ambA)} />)
            break;
        case 3:
            //boton final
            ambA = soloDecimal(ambA)
            return (
                <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorPromedio(ambA) }}>
                    {ambA}
                </Fab>)
            break;
    }
}

function colorPromedio(valorColor) {
    if (valorColor < 0.5) {
        return (cyanA400)
    } else {
        if (valorColor < 1.5) {
            return (lightGreenA400)
        } else {
            if (valorColor < 2.5) {
                return (yellow500)

            } else {
                if (valorColor < 3.5) {
                    return (orange500)
                } else {
                    if (valorColor <= 4) {
                        return (redA700)
                    }
                }
            }
        }
    }

}
function iconCara(lvl) {
    switch (lvl) {
        case 0:
            return (<IconNulo />)
            break;
        case 1:
            return (<IconBajo />)
            break;
        case 2:
            return (<IconMedio />)
            break;
        case 3:
            return (<IconAlto />)
            break;
        case 4:
            return (<IconMuyAlto />)
            break;
    }
}

/**CONDICIONES */

function condiciones(cali) {
    //AMBIENTE

    if (cali < 50) {
        //LinearNulo()
        return (casosA(" NULO ", cali, cyanA400, 0))
    } else {
        if (cali >= 50 && cali < 75) {
            //LinearBajo()
            return (casosA(" BAJO ", cali, lightGreenA400, 1))
        } else {
            if (cali >= 75 && cali < 99) {
                //LinearMedio()
                return (casosA(" MEDIO ", cali, yellow500, 2))
            } else {
                if (cali >= 99 && cali < 140) {
                    //LinearAlto()
                    return (casosA(" ALTO ", cali, orange500, 3))
                } else {
                    if (cali >= 140) {
                        //MuyAlto
                        return (casosA(" MUY ALTO ", cali, redA700, 4))
                    }
                }
            }
        }
    }

}

