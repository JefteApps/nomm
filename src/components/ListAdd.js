import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import TextField from '@material-ui/core/TextField';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
 
var arrDepaFB=[]
var arrDepaFBPro=[]

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
        position: 'relative',
        overflow: 'auto',
        maxHeight: 300,
    },
    listSection: {
        backgroundColor: 'inherit',
    },
    ul: {
        backgroundColor: 'inherit',
        padding: 0,
    },
}));
const classes = useStyles
export default class PinnedSubheaderList extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            depa:"",
            listDepa: []
        }
    }
    render(){
        return (
            <section className={classes.root} style={{ marginTop: '30px'}}>
                <TextField
                    id="id-depa"
                    label="Departamento"
                    className={classes.root}
                    margin="Departamento"
                    value={this.state.depa}
                    onChange={e => this.setState({ depa: e.target.value })}  
                />
                
                <Button
                style={{ marginRight: '20px',marginLeft:'20px'}}
                    className={classes.root}
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={()=>{
                        arrDepaFB.push(this.state.depa)
                        arrDepaFBPro.push(this.state.depa+",")
                        this.setState({
                        
                       // listDepa: [this.state.listDepa,<hr/>,this.state.depa]
                       listDepa:arrDepaFBPro
                    })}}
                >Agregar</Button>
                <Button
                    className={classes.root}
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    onClick={()=>{ this.setState({
                        listDepa: []
                    })}}
                >Eliminar</Button>
                <List className={classes.root} subheader={<li />}>
                    <p>{this.state.listDepa}</p>
                </List>
            </section>
    
        );
    }
    
}