import React from 'react';

import { Card, CardText, CardMedia, CardTitle, CardActions } from 'material-ui/Card';

import FlatButton from 'material-ui/FlatButton';
import RadionButton from 'material-ui/RadioButton';

import {indigo400,redA400,lightBlueA400,amberA400,cyan50} from 'material-ui/styles/colors';

import FadeAndScale from '../animations/FadeAndScale';
import { Radio } from '@material-ui/core';


export default class PlaceCard extends React.Component{
  constructor(props){
      super(props);
      this.state = {
        show: true
      }
  }

  render(){
    return(
      <FadeAndScale  className="col-xs-12 col-sm-12"  in={this.props.in}>
        <div style={{'paddingTop':'15px','paddingBottom':'2px'}}>
          <Card style={{'backgroundColor':cyan50}}>
            <CardTitle title={this.props.place.vrespuesta+". "+this.props.place.pregunta}></CardTitle>
          </Card>
        </div>
      </FadeAndScale>
    );
  }
}
