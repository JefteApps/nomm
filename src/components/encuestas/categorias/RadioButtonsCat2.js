import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {Sumar} from '../../Sumar';


export default function FormControlLabelPosition(props) {
  const [value, setValue] = React.useState('female');

  const handleChange = event => {
    setValue(event.target.value);
  };

  

  return (
    <FormControl component="fieldset">
      <FormLabel component="legend"></FormLabel>
      <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
      <FormControlLabel
          value="Siempre"
          control={<Radio color="primary" />}
          label="Siempre"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,0)}
          
        />
        <FormControlLabel
          value="Casi siempre"
          control={<Radio color="primary" />}
          label="Casi siempre"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,1)}
        />
        <FormControlLabel
          value="Algunas veces"
          control={<Radio color="primary" />}
          label="Algunas veces"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,2)}
        />
        <FormControlLabel
          value="Casi nunca"
          control={<Radio color="primary" />}
          label="Casi nunca"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,3)}
        />
        <FormControlLabel
          value="Nunca"
          control={<Radio color="primary" />}
          label="Nunca"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,4)}
        />
      </RadioGroup>
    </FormControl>
  );
}