import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import {Sumar} from '../../Sumar';
import Button from '@material-ui/core/Button';

export default function FormControlLabelPosition(props) {
  const [value, setValue] = React.useState('female');

  const handleChange = event => {
    setValue(event.target.value);
  };

  

  return (
    <FormControl component="fieldset">
      <FormLabel component="legend"></FormLabel>
      <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>
      <FormControlLabel
          value="Siempre"
          control={<Radio color="primary" />}
          label="Siempre"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,4)}
          
        />
        <FormControlLabel
          value="Casi siempre"
          control={<Radio color="primary" />}
          label="Casi siempre"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,3)}
        />
        <FormControlLabel
          value="Algunas veces"
          control={<Radio color="primary" />}
          label="Algunas veces"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,2)}
        />
        <FormControlLabel
          value="Casi nunca"
          control={<Radio color="primary" />}
          label="Casi nunca"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,1)}
        />
        <FormControlLabel
          value="Nunca"
          control={<Radio color="primary" />}
          label="Nunca"
          labelPlacement="bottom"
          onClick={()=>Sumar(props.index,4)}
        />
      </RadioGroup>
      {
      /*
    <div>
      <Button onClick={()=>Sumar(props.index,4)}
      variant="contained" color="secondary"
      style={{'marginRight':'40px'}}>
      Siempre
      </Button>
      <Button onClick={()=>Sumar(props.index,3)}
      variant="contained" color="secondary"
      style={{'marginRight':'40px'}}>
      Casi iempre
      </Button>
      <Button onClick={()=>Sumar(props.index,2)}
      variant="contained" color="secondary"
      style={{'marginRight':'40px'}}>
      Algunas veces
      </Button>
      <Button onClick={()=>Sumar(props.index,1)}
      variant="contained" color="secondary"
      style={{'marginRight':'40px'}}>
      Casi nunca
      </Button>
      <Button onClick={()=>Sumar(props.index,0)}
      variant="contained" color="secondary"
      style={{'marginRight':'40px'}}>
      Nunca
      </Button>
      
    
    </div>*/}
    </FormControl>

    
  );
}