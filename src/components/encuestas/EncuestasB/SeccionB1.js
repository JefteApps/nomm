import React from 'react';
import Button from '@material-ui/core/Button';
import {indigo400,cyan500,cyan100} from 'material-ui/styles/colors';
import { animateScroll as scroll} from 'react-scroll';
import {
  BrowserRouter as ReactRouter,
  Link
} from 'react-router-dom';
import PlaceCard from '../../places/PlaceCard';
import data1 from '../../../preguntastipoB/SeccionB1';
import {Sumar} from '../../Sumar';

import RadioButtonsCat1 from '../categorias/RadioButtonsCat1';
import RadioButtonsCat2 from '../categorias/RadioButtonsCat2';
import {condicionCategoria,Dominio, condiDominio, condiArray} from '../../Sumar';
import { Card, CardActions } from 'material-ui/Card';

export default class SeccionB1 extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      seccion1: data1.Seccion1
    }
    this.hidePlace = this.hidePlace.bind(this);
  }

  section1(){
    return this.state.seccion1.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        </div>
      );
    })
  }


  hidePlace(place){
    this.setState({
      places: this.state.seccion1.filter(el => el != place),
    })
  }


  render(){
    return(
    <section ref={this.resultsDiv}>
        <section id = "stnBB1" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white'}}>
          <h3 style={{'fontSize': '24px'}}>
            EncuestaB responder las preguntas siguientes considere las condiciones de su centro de trabajo, así como la cantidad y ritmo de trabajo.</h3>
          <div >
            {this.section1()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Link to={"/SeccionB2"}style={{ textDecoration: 'none', color: 'black'}}>
          <Button variant="contained" color="secondary"onClick={scroll.scrollToTop()}>Siguiente</Button>
          </Link>
          </div>
        </section>
    </section>
    );
  }
}
