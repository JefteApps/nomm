import React from 'react';
import Button from '@material-ui/core/Button';
import { indigo400, cyan500 } from 'material-ui/styles/colors';
import { animateScroll as scroll } from 'react-scroll';
import {
  Link
} from 'react-router-dom';
import PlaceCard from '../places/PlaceCard';
import data1 from '../../preguntastipoB/SeccionB1';
import data1B from '../../preguntastipoB/SeccionB1B';
import data1C from '../../preguntastipoB/SeccionB1C';
import data1D from '../../preguntastipoB/SeccionB1D';
import data2 from '../../preguntastipoB/SeccionB2';
import data3 from '../../preguntastipoB/SeccionB3';
import data4 from '../../preguntastipoB/SeccionB4';
import data5 from '../../preguntastipoB/SeccionB5';
import data6 from '../../preguntastipoB/SeccionB6';
import data7 from '../../preguntastipoB/SeccionB7';
import data7B from '../../preguntastipoB/SeccionB7B';
import data8 from '../../preguntastipoB/SeccionB8';
import data9 from '../../preguntastipoB/SeccionB9';
import data10 from '../../preguntastipoB/SeccionB10';
import data11 from '../../preguntastipoB/SeccionB11';
import data11B from '../../preguntastipoB/SeccionB11B';
import data11C from '../../preguntastipoB/SeccionB11C';
import data12 from '../../preguntastipoB/SeccionB12';
import data12B from '../../preguntastipoB/SeccionB12B';
import data13 from '../../preguntastipoB/SeccionB13';
import data14 from '../../preguntastipoB/SeccionB14';
import data15 from '../../preguntastipoB/SeccionB15';
import data16 from '../../preguntastipoB/SeccionB16';
import firebase from '../../Firebase/Firebase';
import { Sumar, SumaValores } from '../Sumar';
import RadioButtonsCat1 from './categorias/RadioButtonsCat1';
import RadioButtonsCat2 from './categorias/RadioButtonsCat2';
import { FinalizarSuma, Linear, condicionCategoria, Dominio, condiDominio, condiArray } from '../Sumar';
import ScrollTop from '../../requests/Scrolltop';
import { Card,CardActions } from 'material-ui/Card';



export function ocultar(objO) {
  document.getElementById(objO).style.display = 'none';
  scroll.scrollToTop();
}
export function mostrar(objM) {
  document.getElementById(objM).style.display = 'block';
}
export function writeUserData(empresa, encuesta, clave) {

}
//https://nom-035.firebaseio.com/JAX/B/1/clave/001/valor

export default class EncuestaB extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      seccion1: data1.Seccion1,
      seccion1B: data1B.Seccion1,
      seccion1C: data1C.Seccion1,
      seccion1D: data1D.Seccion1,
      seccion2: data2.Seccion1,
      seccion3: data3.Seccion1,
      seccion4: data4.Seccion1,
      seccion5: data5.Seccion1,
      seccion6: data6.Seccion1,
      seccion7: data7.Seccion1,
      seccion7B: data7B.Seccion1,
      seccion8: data8.Seccion1,
      seccion9: data9.Seccion1,
      seccion10: data10.Seccion1,
      seccion11: data11.Seccion1,
      seccion11B: data11B.Seccion1,
      seccion11C: data11C.Seccion1,
      seccion12: data12.Seccion1,
      seccion12B: data12B.Seccion1,
      seccion13: data13.Seccion1,
      seccion14: data14.Seccion1,
      seccion15: data15.Seccion1,
      seccion16: data16.Seccion1,
      fb: ""
    }


    this.hidePlace = this.hidePlace.bind(this);
  }

  componentWillMount() {
    const nameRef = firebase.database().ref().child('encuestas').child('Nom035').child('B').child('1').child('pregunta')
    nameRef.on('value', (snapshot) => {
      this.setState({
        fb: snapshot.val()
      })
    })

  }



  section1() {
    return this.state.seccion1.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section1B() {
    return this.state.seccion1B.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 1} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 1} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section1C() {
    return this.state.seccion1C.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 3} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 3} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section1D() {
    return this.state.seccion1D.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 4} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 4} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section2() {
    return this.state.seccion2.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 5} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 5} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section3() {
    return this.state.seccion3.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 8} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 8} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section4() {
    return this.state.seccion4.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 12} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 12} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section5() {
    return this.state.seccion5.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 16} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 16} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section6() {
    return this.state.seccion6.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 22} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 22} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section7() {
    return this.state.seccion7.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 28} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 28} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section7B() {
    return this.state.seccion7B.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 29} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 29} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section8() {
    return this.state.seccion8.map((place, index) => {
      return (
        <div>
          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 30} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 30} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section9() {
    return this.state.seccion9.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 36} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 36} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section10() {
    return this.state.seccion10.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 41} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 41} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section11() {
    return this.state.seccion11.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 46} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 46} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section11B() {
    return this.state.seccion11B.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 53} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 53} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section11C() {
    return this.state.seccion11C.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 54} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 54} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section12() {
    return this.state.seccion12.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 56} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat2 index={index + 56} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section12B() {
    return this.state.seccion12B.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 57} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 57} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }
  section13() {
    return this.state.seccion13.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <button>SI</button>
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }

  section14() {
    return this.state.seccion14.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 64} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 64} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }

  section15() {
    return this.state.seccion15.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>

              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }

  section16() {
    return this.state.seccion16.map((place, index) => {
      return (
        <div>

          <PlaceCard onRemove={this.hidePlace} place={place} index={index + 68} />
          <Card style={{ 'backgroundColor': cyan500, 'color': 'white', 'textAlign': 'right', 'border-radius': '15px' }}>
            <CardActions>
              <div>
                <RadioButtonsCat1 index={index + 68} />
              </div>
            </CardActions>
          </Card>

        </div>
      );
    })
  }



  hidePlace(place) {
    this.setState({
      places: this.state.seccion1.filter(el => el != place),
      places: this.state.seccion2.filter(el => el != place),
      places: this.state.seccion3.filter(el => el != place),
      places: this.state.seccion4.filter(el => el != place),
      places: this.state.seccion5.filter(el => el != place),
      places: this.state.seccion6.filter(el => el != place),
      places: this.state.seccion7.filter(el => el != place),
      places: this.state.seccion8.filter(el => el != place),
      places: this.state.seccion9.filter(el => el != place),
      places: this.state.seccion10.filter(el => el != place),
      places: this.state.seccion11.filter(el => el != place),
      places: this.state.seccion12.filter(el => el != place),
      places: this.state.seccion13.filter(el => el != place),
      places: this.state.seccion14.filter(el => el != place),
      places: this.state.seccion15.filter(el => el != place),
      places: this.state.seccion16.filter(el => el != place)

    })
  }

  checkServicioyJefe(servi, jefe, lugar) {
    switch (lugar) {
      case 1://stnB12 siguiente
        if (servi) {
          ocultar("stnB12")
          mostrar("stnB14")
        } else {
          if (jefe) {
            ocultar("stnB12")
            mostrar("stnB16")
          } else {
            ocultar("stnB12")
            alert("Fin de la encuesta")
          }
        }
        break;
      case 2://stnB14 siguiente
        if (jefe) {
          ocultar("stnB14")
          mostrar("stnB16")
        } else {
          ocultar("stnB14")
          alert("Fin de la encuesta")
        }
        break;
      case 3://stnB16 atras
        if (servi) {
          ocultar("stnB16")
          mostrar("stnB14")
        } else {
          ocultar("stnB16")
          mostrar("stnB12")
        }
        break;
    }

    if (lugar == 1) {

    } else {

    }

  }



  render() {
    return (
      <section ref={this.resultsDiv}>

        <section id="stnB1" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            {//this.state.fb
            }
            Para responder las preguntas siguientes considere las condiciones ambientales de su centro de trabajo.
          </h3>
          <div >
            {this.section1()}
            {this.section1B()}
            {this.section1C()}
            {this.section1D()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { writeUserData("001", "Erick", "a@1.com", "miImagen.jpg"), ocultar("stnB1"), mostrar("stnB2") }}>Siguiente</Button>
            {
              //
              //<Button  variant="contained" color="secondary" onClick={(event)=>{Dominio(),condicionCategoria()}}>FIN</Button>
            }
          </div>
        </section>

        <section id="stnB2" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Para responder a las preguntas siguientes piense en la cantidad y ritmo de trabajo que tiene.
            </h3>
          <div >
            {this.section2()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB2"), mostrar("stnB1") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB2"), mostrar("stnB3") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB3" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con el esfuerzo mental que le exige su trabajo.
            </h3>
          <div >
            {this.section3()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB3"), mostrar("stnB2") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB3"), mostrar("stnB4") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB4" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con las actividades que realiza en su trabajo y las responsabilidades que tiene.
            </h3>
          <div >
            {this.section4()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB4"), mostrar("stnB3") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB4"), mostrar("stnB5") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB5" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con su jornada de trabajo.
            </h3>
          <div >
            {this.section5()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB5"), mostrar("stnB4") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB5"), mostrar("stnB6") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB6" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con las decisiones que puede tomar en su trabajo.
            </h3>
          <div >
            {this.section6()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB6"), mostrar("stnB5") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB6"), mostrar("stnB7") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB7" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con cualquier tipo de cambio que ocurra en su trabajo (considere los últimos cambios realizados).
            </h3>
          <div >
            {this.section7()}
            {this.section7B()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB7"), mostrar("stnB6") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB7"), mostrar("stnB8") }}>Siguiente</Button>
          </div>

        </section>

        <section id="stnB8" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con la capacitación e información que se le proporciona sobre su trabajo.
            </h3>
          <div >
            {this.section8()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB8"), mostrar("stnB7") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB8"), mostrar("stnB9") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB9" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con el o los jefes con quien tiene contacto.
            </h3>
          <div >
            {this.section9()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB9"), mostrar("stnB8") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB9"), mostrar("stnB10") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB10" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes se refieren a las relaciones con sus compañeros.
            </h3>
          <div >
            {this.section10()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB10"), mostrar("stnB9") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB10"), mostrar("stnB11") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB11" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con la información que recibe sobre su rendimiento en el trabajo, el reconocimiento, el sentido de pertenencia y la estabilidad que le ofrece su trabajo.
            </h3>
          <div >
            {this.section11()}
            {this.section11B()}
            {this.section11C()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB11"), mostrar("stnB10") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB11"), mostrar("stnB12") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB12" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con actos de violencia laboral (malos tratos, acoso, hostigamiento, acoso psicológico).
            </h3>
          <div >
            {this.section12()}
            {this.section12B()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB12"), mostrar("stnB11") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { this.checkServicioyJefe(this.props.checkServi, this.props.checkJefe, 1) }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB13" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.
            </h3>
          <div >
            {this.section13()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB13"), mostrar("stnB12") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB13"), mostrar("stnB14") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB14" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con la atención a clientes y usuarios.
            </h3>
          <div >
            {this.section14()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB14"), mostrar("stnB12") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { this.checkServicioyJefe(this.props.checkServi, this.props.checkJefe, 2) }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB15" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con las actitudes de las personas que supervisa.
            </h3>
          <div >
            {this.section15()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB15"), mostrar("stnB14") }}>Atras</Button>
            <Button variant="contained" color="secondary" onClick={(event) => { ocultar("stnB15"), mostrar("stnB16") }}>Siguiente</Button>
          </div>
        </section>

        <section id="stnB16" style={{ 'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display': 'none' }}>
          <h3 style={{ 'fontSize': '24px' }}>
            Las preguntas siguientes están relacionadas con las actitudes de las personas que supervisa.
            </h3>
          <div >
            {this.section16()}
          </div>
          <div style={{ 'textAlign': 'right', 'paddingTop': '40px' }}>
            <Button variant="contained" color="secondary" onClick={(event) => { this.checkServicioyJefe(this.props.checkServi, this.props.checkJefe, 3) }}>Atras</Button>
            {//<Button  variant="contained" color="secondary" onClick={(event)=>{ocultar("stnB16"),condicionCategoria()}}>Finalizar</Button>
              //<Button  variant="contained" color="secondary" onClick={(event)=>{Dominio(),condicionCategoria()}}>Finalizar</Button>
            }
            <Link to={"/Gracias"} style={{ textDecoration: 'none', color: 'black'}} onClick={(event) => { 
              Dominio(),
              condicionCategoria(),
              condiDominio(),
              condiArray(),
              SumaValores(this.props.idEmpresa, this.props.idEncuesta, this.props.idClave, this.props.departamento)    
             }}>
              <Button variant="contained" color="secondary">Finalizar</Button>
            </Link>

          </div>
        </section>

        <Link to={"/Gracias"} style={{ textDecoration: 'none', color: 'black'}} onClick={(event) => {
        aleatorioEncuesta(),
          Dominio(),
          condicionCategoria(),
          condiDominio(),
          condiArray(),
          SumaValores(this.props.idEmpresa, this.props.idEncuesta, this.props.idClave, this.props.departamento)
         
        }}>
          <Button variant="contained" color="secondary">Aleatorio</Button>
        </Link>
      </section>
    );
  }

}

function aleatorioEncuesta() {
  var rnmV
  for (var i = 0; i < 72; i++) {
    rnmV = Math.floor(Math.random() * 4);
    if (rnmV <= 2) {
      Sumar(i, rnmV)
    } else {
      i--
    }
  }
}