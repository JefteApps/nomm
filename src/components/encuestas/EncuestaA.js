import React from 'react';

import Button from '@material-ui/core/Button';


import {indigo400,cyan500,cyan100} from 'material-ui/styles/colors';
import { animateScroll as scroll} from 'react-scroll';


import PlaceCard from '../places/PlaceCard';
import data1 from '../../requests/Seccion1';
import data2 from '../../requests/Seccion2';
import data3 from '../../requests/Seccion3';
import data4 from '../../requests/Seccion4';
import data5 from '../../requests/Seccion5';
import data6 from '../../requests/Seccion6';
import data7 from '../../requests/Seccion7';
import data8 from '../../requests/Seccion8';
import RadioButtonsCat1 from './categorias/RadioButtonsCat1';
import RadioButtonsCat2 from './categorias/RadioButtonsCat2';

import {FinalizarSuma} from '../Sumar';

import Chart from 'react-google-charts';
import ScrollTop from '../../requests/Scrolltop';
import { Card, CardText, CardMedia, CardTitle, CardActions } from 'material-ui/Card';
import { fromBase64 } from 'bytebuffer';

export default class EncuestaA extends React.Component{

  constructor(props){
    super(props);

    this.state = {
      seccion1: data1.Seccion1,
      seccion2: data2.Seccion2,
      seccion3: data3.Seccion3,
      seccion4: data4.Seccion4,
      seccion5: data5.Seccion5,
      seccion6: data6.Seccion6,
      seccion7: data7.Seccion7,
      seccion8: data8.Seccion8
    }


    this.hidePlace = this.hidePlace.bind(this);
  }

  section1(){
    return this.state.seccion1.map((place,index)=>{
      return(
        <div>
          
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section2(){
    return this.state.seccion2.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section3(){
    return this.state.seccion3.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section4(){
    return this.state.seccion4.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat2 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section5(){
    return this.state.seccion5.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat2 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section6(){
    return this.state.seccion6.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat2 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section7(){
    return this.state.seccion7.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }
  section8(){
    return this.state.seccion8.map((place,index)=>{
      return(
        <div>
        <PlaceCard onRemove={this.hidePlace} place={place} index={index}/>
        <Card style={{'backgroundColor':cyan500,'color':'white','textAlign': 'right', 'border-radius': '15px'}}>
            <CardActions>
            <div>
            <RadioButtonsCat1 index={index}/> 
            </div>
            </CardActions>
          </Card>
        
        </div>
      );
    })
  }


  hidePlace(place){
    this.setState({
      places: this.state.seccion1.filter(el => el != place),
      places: this.state.seccion2.filter(el => el != place),
      places: this.state.seccion3.filter(el => el != place),
      places: this.state.seccion4.filter(el => el != place),
      places: this.state.seccion5.filter(el => el != place),
      places: this.state.seccion6.filter(el => el != place),
      places: this.state.seccion7.filter(el => el != place),
      places: this.state.seccion8.filter(el => el != place)

    })
  }
  ocultar(objO){
    document.getElementById(objO).style.display = 'none';
    scroll.scrollToTop();
  }
  mostrar(objM){
    document.getElementById(objM).style.display = 'block';
  }

  render(){
    return(
      <section ref={this.resultsDiv}>
        
        <section id = "stn1" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white'}}>
          <h3 style={{'fontSize': '24px'}}>Para responder las preguntas siguientes considere las condiciones de su centro de trabajo, así como la cantidad y ritmo de trabajo.</h3>
          <div >
            {this.section1()}
            
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          
          {//
            }
            <Button  variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn1"),this.mostrar("stn2")}}>Siguiente</Button>
            
          </div>
          
          
        </section>

        <section id = "stn2" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white', 'display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes están relacionadas con las actividades que realiza en su trabajo y las responsabilidades que tiene.</h3>
          <div >
            {this.section2()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn2"),this.mostrar("stn1")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn2"),this.mostrar("stn3")}}>Siguiente</Button>
          </div>
        </section>

        <section id = "stn3" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes están relacionadas con el tiempo destinado a su trabajo y sus responsabilidades familiares.</h3>
          <div >
            {this.section3()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn3"),this.mostrar("stn2")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn3"),this.mostrar("stn4")}}>Siguiente</Button>
          </div>
        </section>

        <section id = "stn4" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes están relacionadas con las decisiones que puede tomar en su trabajo.</h3>
          <div >
            {this.section4()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn4"),this.mostrar("stn3")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn4"),this.mostrar("stn5")}}>Siguiente</Button>
          </div>
        </section>

        <section id = "stn5" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes están relacionadas con la capacitación e información que recibe sobre su trabajo.</h3>
          <div >
            {this.section5()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn5"),this.mostrar("stn4")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn5"),this.mostrar("stn6")}}>Siguiente</Button>
          </div>
        </section>

        <section id = "stn6" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes se refieren a las relaciones con sus compañeros de trabajo y su jefe.</h3>
          <div >
            {this.section6()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn6"),this.mostrar("stn5")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn6"),this.mostrar("stn7")}}>Siguiente</Button>
          </div>
        </section>

        <section id = "stn7" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las preguntas siguientes se refieren a las relaciones con sus compañeros de trabajo y su jefe.</h3>
          <div >
            {this.section7()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn7"),this.mostrar("stn6")}}>Atras</Button>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn7"),this.mostrar("stn8")}}>Siguiente</Button>
          </div>
          
        </section>

        <section id = "stn8" style={{'backgroundColor': indigo400, 'padding': '50px', color: 'white','display':'none'}}>
          <h3 style={{'fontSize': '24px'}}>Las siguientes preguntas están relacionadas con las actitudes de los trabajadores que supervisa.</h3>
          <div >
            {this.section8()}
          </div>
          <div style={{'textAlign': 'right' ,'paddingTop':'40px'}}>
          <Button variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn8"),this.mostrar("stn7")}}>Atras</Button>
          <Button  variant="contained" color="secondary" onClick={(event)=>{this.ocultar("stn8"),this.mostrar("graf"),FinalizarSuma()}}>Finalizar</Button>
          
          
          </div>
          
        </section>
        <section id = "graf" style={{'display':'none'}}>
          
        </section>
      </section>
    );
  }

}
