import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {mostrar,ocultar} from './encuestas/EncuestaB';

var objO,objM,objA

const useStyles = makeStyles(theme => ({
  root: {
    width: '90%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps() {
  return ['Sección 1', 
  'Sección 2', 
  'Sección 3'
  , 'Sección 4'
  , 'Sección 5'
  , 'Sección 6'
  , 'Sección 7'
  , 'Sección 8'
  , 'Sección 9'
  , 'Sección 10'
  , 'Sección 11'
  , 'Sección 12'
  , 'Sección 13'
  , 'Sección 14'
  , 'Sección 15'
  , 'Sección 16'];
}

function getStepContent(stepIndex) {
    switch (stepIndex) {
        case 0:
            objO="stnB1"
            objM="stnB2"
          return 'Sección 1';
        case 1:
                objA="stnB1"
                objO="stnB2"
                objM="stnB3"
          return 'Sección 2';
        case 2:
                objA="stnB2"
                objO="stnB3"
                objM="stnB4"
          return 'Sección 3';
        case 3:
                objA="stnB3"
                objO="stnB4"
                objM="stnB5"
          return 'Sección 4';
        case 4:
                objA="stnB4"
                objO="stnB5"
                objM="stnB6"
          return 'Sección 5';
        case 5:
                objA="stnB5"
                objO="stnB6"
                objM="stnB7"
          return 'Sección 6';
        case 6:
                objA="stnB6"
                objO="stnB7"
                objM="stnB8"
          return 'Sección 7';
        case 7:
                objA="stnB7"
                objO="stnB8"
                objM="stnB9"
          return 'Sección 8';
        case 8:
                objA="stnB8"
                objO="stnB9"
                objM="stnB10"
          return 'Sección 9';
        case 9:
                objA="stnB9"
                objO="stnB10"
                objM="stnB11"
          return 'Sección 10';
        case 10:
                objA="stnB10"
                objO="stnB11"
                objM="stnB12"
          return 'Sección 11';
        case 11:
                objA="stnB11"
                objO="stnB12"
                objM="stnB13"
          return 'Sección 12';
        case 12:
                objA="stnB12"
                objO="stnB13"
                objM="stnB14"
          return 'Sección 13';
        case 13:
                objA="stnB13"
                objO="stnB14"
                objM="stnB15"
          return 'Sección 14';
        case 14:
                objA="stnB14"
                objO="stnB15"
                objM="stnB16"
          return 'Sección 15';
        case 15:
                objA="stnB15"
                objO="stnB16"
                objM="grafB"
          return 'Sección 16';
        default:
          return 'Uknown stepIndex';
      }
}

export default function HorizontalLabelPositionBelowStepper() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = React.useState(0);
  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  return (
    <div className={classes.root}>
      <Stepper activeStep={activeStep} alternativeLabel>
        {steps.map(label => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>
      <div>
        {activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>Completado</Typography>
            <Button onClick={handleReset}>Reinicio</Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>{getStepContent(activeStep)}</Typography>
            <div>
              <Button
                disabled={activeStep === 0}
                onClick={(e)=>{handleBack,mostrar(objA),ocultar(objO)}}
                className={classes.backButton}
              >
                Atras
              </Button>
              <Button variant="contained" color="primary" onClick={(e)=>{handleNext,mostrar(objM),ocultar(objO)}}>
                {activeStep === steps.length - 1 ? 'Finalizar' : 'Siguiente'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}