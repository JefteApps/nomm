import React from 'react';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { redA700, orange500, yellow500, lightGreenA400, cyanA400, green50 } from 'material-ui/styles/colors';
import Fab from '@material-ui/core/Fab';
import IconNulo from '@material-ui/icons/SentimentVerySatisfied';
import IconBajo from '@material-ui/icons/SentimentSatisfiedAlt';
import IconMedio from '@material-ui/icons/SentimentSatisfied';
import IconAlto from '@material-ui/icons/SentimentDissatisfied';
import IconMuyAlto from '@material-ui/icons/SentimentVeryDissatisfied';
import firebase from '../Firebase/Firebase';

var idMZ = 0
var contI = 1,contGlobal=0

export default class SimpleTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      vAmbiente: 0,
      vFactores: 0,
      vOrg: 0,
      vLiderCat: 0,
      vEntorno: 0,
      vCondiciones: 0,
      vCarga: 0,
      vFalta: 0,
      vJornada: 0,
      vInter: 0,
      vLider: 0,
      vRelaciones: 0,
      vViolencia: 0,
      vReconocimiento: 0,
      vInsuficiente: 0,
      di1: 0,
      di2: 0,
      di3: 0,
      di4: 0,
      di5: 0,
      di6: 0,
      di7: 0,
      di8: 0,
      di9: 0,
      di10: 0,
      di11: 0,
      di12: 0,
      di13: 0,
      di14: 0,
      di15: 0,
      di16: 0,
      di17: 0,
      di18: 0,
      di19: 0,
      di20: 0,
      di21: 0,
      di22: 0,
      di23: 0,
      di24: 0,
      di25: 0
    }
  }

  componentWillMount() {
    try {
      var nameRefX
      idMZ = this.props.idLugar
      // alert("tipo "+idMZ)
      switch (idMZ) {
        //General
        case "1": nameRefX = firebase.database().ref()
          .child('empresa')
          .child('' + this.props.idEmpresa)
          .child('' + this.props.idEncuesta)
          .child('general')
          nameRefX.child('cont').on('value', (snapshot) => {
            contI = snapshot.val()
          })
          break
        //Departamento
        case "2": nameRefX = firebase.database().ref()
          .child('empresa')
          .child('' + this.props.idEmpresa)
          .child('' + this.props.idEncuesta)
          .child('depa')
          .child('' + this.props.idDepa)
          nameRefX.child('cont').on('value', (snapshot) => {
            contI = snapshot.val()
          })
          break
        //Folio/Clave
        case "3": nameRefX = firebase.database().ref()
          .child('empresa')
          .child('' + this.props.idEmpresa)
          .child('' + this.props.idEncuesta)
          .child('clave')
          .child('' + this.props.idClave)
          contI = 1
          break

      }
      //alert("mi ruta es "+nameRefX)
      nameRefX.on('value', (snapshot) => {
        this.setState({
          vAmbiente: soloDecimal(snapshot.val().ambiente / contI),
          vFactores: soloDecimal(snapshot.val().factores / contI),
          vOrg: soloDecimal(snapshot.val().org / contI),
          vLiderCat: soloDecimal(snapshot.val().liderCat / contI),
          vEntorno: soloDecimal(snapshot.val().entorno / contI),
          vCondiciones: soloDecimal(snapshot.val().condiciones / contI),
          vCarga: soloDecimal(snapshot.val().carga / contI),
          vFalta: soloDecimal(snapshot.val().falta / contI),
          vJornada: soloDecimal(snapshot.val().jornada / contI),
          vInter: soloDecimal(snapshot.val().inter / contI),
          vLider: soloDecimal(snapshot.val().lider / contI),
          vRelaciones: soloDecimal(snapshot.val().relaciones / contI),
          vViolencia: soloDecimal(snapshot.val().violencia / contI),
          vReconocimiento: soloDecimal(snapshot.val().reconocimiento / contI),
          vInsuficiente: soloDecimal(snapshot.val().insuficiente / contI),
          di1: soloDecimal(snapshot.val().di1 / contI),
          di2: soloDecimal(snapshot.val().di2 / contI),
          di3: soloDecimal(snapshot.val().di3 / contI),
          di4: soloDecimal(snapshot.val().di4 / contI),
          di5: soloDecimal(snapshot.val().di5 / contI),
          di6: soloDecimal(snapshot.val().di6 / contI),
          di7: soloDecimal(snapshot.val().di7 / contI),
          di8: soloDecimal(snapshot.val().di8 / contI),
          di9: soloDecimal(snapshot.val().di9 / contI),
          di10: soloDecimal(snapshot.val().di10 / contI),
          di11: soloDecimal(snapshot.val().di11 / contI),
          di12: soloDecimal(snapshot.val().di12 / contI),
          di13: soloDecimal(snapshot.val().di13 / contI),
          di14: soloDecimal(snapshot.val().di14 / contI),
          di15: soloDecimal(snapshot.val().di15 / contI),
          di16: soloDecimal(snapshot.val().di16 / contI),
          di17: soloDecimal(snapshot.val().di17 / contI),
          di18: soloDecimal(snapshot.val().di18 / contI),
          di19: soloDecimal(snapshot.val().di19 / contI),
          di20: soloDecimal(snapshot.val().di20 / contI),
          di21: soloDecimal(snapshot.val().di21 / contI),
          di22: soloDecimal(snapshot.val().di22 / contI),
          di23: soloDecimal(snapshot.val().di23 / contI),
          di24: soloDecimal(snapshot.val().di24 / contI),
          di25: soloDecimal(snapshot.val().di25 / contI)
        })
        /**
        alert("-"+this.state.vCondiciones+"-"+
        this.state.vCarga+"-"+
        this.state.vFalta+"-"+
        this.state.vJornada+"-"+
        this.state.vInter+"-"+
        this.state.vLider+"-"+
        this.state.vRelaciones+"-"+
        this.state.vViolencia+"-"+
        this.state.vReconocimiento+"-"+
        this.state.vInsuficiente)
         */

        //alert("valor d1 "+snapshot.val().di1+contI)

      })
      const nameRefGeneral = firebase.database().ref()
        .child('empresa')
        .child('' + this.props.idEmpresa)
        .child('' + this.props.idEncuesta)


      nameRefGeneral.child('cont').on('value', (snapshot) => {
        contGlobal = snapshot.val().cont
      })
    }
    catch (error) {
      console.error(error);
      // expected output: ReferenceError: nonExistentFunction is not defined
      // Note - error messages will vary depending on browser
    }

  }


  render() {
    return (
      <Paper style={{backgroundColor:green50}}>
        <Table >
          <TableHead>
            <TableRow>
              <TableCell style={{ width: 300 }} align="center">Categoria</TableCell>
              <TableRow>
                <TableCell style={{ width: 50 }} align="center"></TableCell>
                <TableCell style={{ width: 210 }} align="center">Dominio</TableCell>
                <TableCell style={{ width: 400 }} align="center">Dimensión</TableCell>
                <TableCell style={{ width: 100 }} align="center">Sumatoria</TableCell>
                <TableCell style={{ width: 100 }} align="center">Promedio</TableCell>
              </TableRow>
            </TableRow>
          </TableHead>
          <TableBody>
            {//Categoria Ambiente
            }
            <TableRow >
              <TableCell align="center" style={{ width: 300 }}>{condiAmbiental(this.state.vAmbiente, 0, "Ambiente de trabajo")}
              </TableCell>
              <TableCell>
                <TableRow >
                  <TableCell align="center" style={{ width: 50 }}>{condiCondi(this.state.vCondiciones)}</TableCell>
                  <TableCell align="center" style={{ width: 200 }}>Condiciones en el ambiente de trabajo</TableCell>
                  <TableCell align="left" style={{ width: 400 }}>
                    <p>Condiciones peligrosas e inseguras</p>{casosA(2, "", (this.state.di1 / 2))}
                    <p>Condiciones deficientes e insalubres</p>{casosA(2, "", (this.state.di2 / 2))}
                    <p>Trabajo Peligroso</p>{casosA(2, "", (this.state.di3))}
                  </TableCell>
                  <TableCell align="center" style={{ width: 100 }}>
                    <p>{this.state.di1}</p><hr />
                    <p>{this.state.di2}</p><hr />
                    <p>{this.state.di3}</p>
                  </TableCell>
                  <TableCell align="center" style={{ width: 100 }}>
                    {casosA(3, "", (this.state.di1 / 2))}<hr />
                    {casosA(3, "", (this.state.di2 / 2))}<hr />
                    {casosA(3, "", (this.state.di3))}
                  </TableCell>
                </TableRow>
              </TableCell>
            </TableRow>
            {//Cat Factores
            }
            <TableRow>
              <TableCell style={{ width: 300 }}>{condiFactores(this.state.vFactores, 0, "Factores propios de la actividad")}</TableCell>
              <TableCell>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiCarga(this.state.vCarga)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Carga de trabajo</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Cargas cuantitativas</p>{casosA(2, "", (this.state.di4 / 2))}
                    <p>Ritmos de trabajo acelerado</p> {casosA(2, "", (this.state.di5 / 2))}
                    <p>Carga mental</p>{casosA(2, "", (this.state.di6 / 3))}
                    <p>Cargas psicológicas emocionales</p>{casosA(2, "", (this.state.di7 / 4))}
                    <p>Cargas de alta responsabilidad</p>{casosA(2, "", (this.state.di8 / 2))}
                    <p>Cargas contradictorias o inconsistentes</p>{casosA(2, "", (this.state.di9 / 2))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di4}</p><hr />
                    <p>{this.state.di5}</p><hr />
                    <p>{this.state.di6}</p><hr />
                    <p>{this.state.di7}</p><hr />
                    <p>{this.state.di8}</p><hr />
                    <p>{this.state.di9}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di4 / 2))}<hr />
                    {casosA(3, "", (this.state.di5 / 2))}<hr />
                    {casosA(3, "", (this.state.di6 / 3))}<hr />
                    {casosA(3, "", (this.state.di7 / 4))}<hr />
                    {casosA(3, "", (this.state.di8 / 2))}<hr />
                    {casosA(3, "", (this.state.di9 / 2))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiFalta(this.state.vFalta)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Falta de control sobre el trabajo</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Falta de control y autonomía sobre el trabajo</p> {casosA(2, "", (this.state.di10 / 4))}
                    <p>Limitada o nula posibilidad de desarrollo</p>{casosA(2, "", (this.state.di11 / 2))}
                    <p>Insuficiente participación y manejo del cambio</p> {casosA(2, "", (this.state.di12 / 2))}
                    <p>Limitada o inexistente capacitación</p>{casosA(2, "", (this.state.di13 / 2))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di10}</p><hr />
                    <p>{this.state.di11}</p><hr />
                    <p>{this.state.di12}</p><hr />
                    <p>{this.state.di13}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di10 / 4))}<hr />
                    {casosA(3, "", (this.state.di11 / 2))}<hr />
                    {casosA(3, "", (this.state.di12 / 2))}<hr />
                    {casosA(3, "", (this.state.di13 / 2))}
                  </TableCell>
                </TableRow>
              </TableCell>
            </TableRow>
            {//Cat Organización
            }
            <TableRow>
              <TableCell style={{ width: 300 }}> {condiOrg(this.state.vOrg, 0, "Organización del tiempo de trabajo")}</TableCell>
              <TableCell>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiJornada(this.state.vJornada)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Jornada de trabajo</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Jornadas de trabajo extensas</p> {casosA(2, "", (this.state.di14 / 2))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di14}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di14 / 2))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiInter(this.state.vInter)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Interferencia en la relación trabajo-familia</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Influencia del trabajo fuera del centro laboral</p>{casosA(2, "", (this.state.di15 / 2))}
                    <p>Influencia de las responsabilidades familiares</p>{casosA(2, "", (this.state.di16 / 2))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di15}</p><hr />
                    <p>{this.state.di16}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di15 / 2))}<hr />
                    {casosA(3, "", (this.state.di16 / 2))}
                  </TableCell>
                </TableRow>
              </TableCell>
            </TableRow>
            {//Cat Liderazgo
            }
            <TableRow>
              <TableCell style={{ width: 300 }}>{condiLider(this.state.vLiderCat, 0, "Liderazgo y relaciones en el trabajo")}</TableCell>
              <TableCell>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiLiderD(this.state.vLider)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Liderazgo</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Escaza claridad de funciones</p>  {casosA(2, "", (this.state.di17 / 4))}
                    <p>Características del liderazgo</p>{casosA(2, "", (this.state.di18 / 5))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di17}</p><hr />
                    <p>{this.state.di18}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di17 / 4))}<hr />
                    {casosA(3, "", (this.state.di18 / 5))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiRelaciones(this.state.vRelaciones, 1)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Relaciones en el trabajo</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Relaciones sociales en el trabajo</p> {casosA(2, "", (this.state.di19 / 5))}
                    <p>Deficiente relación con los colaboradores que supervisa</p>{casosA(2, "", (this.state.di20 / 4))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di19}</p><hr />
                    <p>{this.state.di20}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di19 / 5))}<hr />
                    {casosA(3, "", (this.state.di20 / 4))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiViolencia(this.state.vViolencia)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Violencia</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p>Violencia laboral</p>
                    {casosA(2, "", (this.state.di21 / 8))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di21}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di21 / 8))}
                  </TableCell>
                </TableRow>
              </TableCell>
            </TableRow>
            {//Categoria Entorno
            }
            <TableRow>
              <TableCell style={{ width: 300 }}>{condiEntorno(this.state.vEntorno, 0, "Entorno organizacional")}</TableCell>
              <TableCell>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiReconocimiento(this.state.vReconocimiento)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Reconocimiento del desempeño</TableCell>
                  <TableCell style={{ width: 400 }}>
                    <p >Escasa o nula retroalimentación del desempeño</p>
                    {casosA(2, "", (this.state.di22 / 2))}
                    <p>Escaso o nulo reconocimiento y compensación</p>
                    {casosA(2, "", (this.state.di23 / 4))}
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    <p>{this.state.di22}</p><hr />
                    <p>{this.state.di23}</p>
                  </TableCell>
                  <TableCell style={{ width: 100 }} align="center">
                    {casosA(3, "", (this.state.di22 / 2))}<hr />
                    {casosA(3, "", (this.state.di23 / 4))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell align="center" style={{ width: 50 }}>{condiInsuficiente(this.state.vInsuficiente)}</TableCell>
                  <TableCell style={{ width: 200 }} align="center">Insuficiente sentido de pertenencia e, inestabilidad</TableCell>
                  <TableCell style={{ width: 400, height: 200 }}>
                    <p>Limitado sentido de pertenencia</p>{casosA(2, "", (this.state.di24 / 2))}
                    <p>Inestabilidad laboral</p>{casosA(2, "", (this.state.di25 / 2))}
                  </TableCell>
                  <TableCell style={{ width: 100, height: 200 }} align="center">
                    <p>{this.state.di24}</p><hr />
                    <p>{this.state.di25}</p>
                  </TableCell>
                  <TableCell style={{ width: 100, height: 200 }} align="center">
                    {casosA(3, "", (this.state.di24 / 2))}<hr />
                    {casosA(3, "", (this.state.di25 / 2))}
                  </TableCell>
                </TableRow>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Paper>
    );
  }
}


function casosA(lgA, valorA, ambA, colorA, lvl) {
  switch (lgA) {
    case 0:
      return (
        //Primer boton
        <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorA }}>
          {valorA}
        </Fab>)
      break;
    case 1:
      //Segundo boton con carita
      return (
        <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorA }}>
          {iconCara(lvl)}</Fab>)
      break;
    case 2:
      //Raya de color en el texto

      return (<hr color={colorPromedio(ambA)} />)
      break;
    case 3:
      //boton final
      ambA = soloDecimal(ambA)
      return (
        <Fab variant="extended" aria-label="add" style={{ 'backgroundColor': colorPromedio(ambA) }}>
          {ambA}
        </Fab>)
      break;
  }
}
function soloDecimal(valorInicial) {
  var parteEntera, resultado;
  resultado = valorInicial;
  parteEntera = Math.floor(resultado);
  resultado = (resultado - parteEntera) * Math.pow(10, 1);
  resultado = Math.round(resultado);
  resultado = (resultado / Math.pow(10, 1)) + parteEntera;
  return resultado;
}
function colorPromedio(valorColor) {
  if (valorColor < 0.5) {
    return (cyanA400)
  } else {
    if (valorColor < 1.5) {
      return (lightGreenA400)
    } else {
      if (valorColor < 2.5) {
        return (yellow500)

      } else {
        if (valorColor < 3.5) {
          return (orange500)
        } else {
          if (valorColor <= 4) {
            return (redA700)
          }
        }
      }
    }
  }

}
function iconCara(lvl) {
  switch (lvl) {
    case 0:
      return (<IconNulo />)
      break;
    case 1:
      return (<IconBajo />)
      break;
    case 2:
      return (<IconMedio />)
      break;
    case 3:
      return (<IconAlto />)
      break;
    case 4:
      return (<IconMuyAlto />)
      break;
  }
}

/**CONDICIONES */

function condiAmbiental(cali, lugar, texto) {
  //AMBIENTE

  if (cali < 5) {
    //LinearNulo()
    return (casosA(lugar, texto, cali, cyanA400, 0))
  } else {
    if (cali >= 5 && cali < 9) {
      //LinearBajo()
      return (casosA(lugar, texto, cali, lightGreenA400, 1))
    } else {
      if (cali >= 9 && cali < 11) {
        //LinearMedio()
        return (casosA(lugar, texto, cali, yellow500, 2))
      } else {
        if (cali >= 11 && cali < 14) {
          //LinearAlto()
          return (casosA(lugar, texto, cali, orange500, 3))
        } else {
          if (cali >= 14) {
            //MuyAlto
            return (casosA(lugar, texto, cali, redA700, 4))
          }
        }
      }
    }
  }

}

function condiFactores(cali, lugar, texto) {
  //Factores
  if (cali < 15) {
    //LinearNulo()
    return (casosA(lugar, texto, cali, cyanA400, 0))
  } else {
    if (cali < 30) {
      //LinearBajo()
      return (casosA(lugar, texto, cali, lightGreenA400, 1))
    } else {
      if (cali < 45) {
        //LinearMedio()
        return (casosA(lugar, texto, cali, yellow500, 2))
      } else {
        if (cali < 60) {
          //LinearAlto()
          return (casosA(lugar, texto, cali, orange500, 3))
        } else {
          if (cali >= 60) {
            //MuyAlto
            return (casosA(lugar, texto, cali, redA700, 4))
          }
        }
      }
    }
  }
}

function condiOrg(cali, lugar, texto) {
  //Organización
  if (cali < 5) {
    //LinearNulo()
    return (casosA(lugar, texto, cali, cyanA400, 0))
  } else {
    if (cali < 7) {
      //LinearBajo()
      return (casosA(lugar, texto, cali, lightGreenA400, 1))
    } else {
      if (cali < 10) {
        //LinearMedio()
        return (casosA(lugar, texto, cali, yellow500, 2))
      } else {
        if (cali < 13) {
          //LinearAlto()
          return (casosA(lugar, texto, cali, orange500, 3))
        } else {
          if (cali >= 13) {
            //MuyAlto
            return (casosA(lugar, texto, cali, redA700, 4))
          }
        }
      }
    }
  }
}

function condiLider(cali, lugar, texto) {
  //Liderazgo
  if (cali < 14) {
    //LinearNulo()
    return (casosA(lugar, texto, cali, cyanA400, 0))
  } else {
    if (cali < 29) {
      //LinearBajo()
      return (casosA(lugar, texto, cali, lightGreenA400, 1))
    } else {
      if (cali < 42) {
        //LinearMedio()
        return (casosA(lugar, texto, cali, yellow500, 2))
      } else {
        if (cali < 58) {
          //LinearAlto()
          return (casosA(lugar, texto, cali, orange500, 3))
        } else {
          if (cali >= 58) {
            //MuyAlto
            return (casosA(lugar, texto, cali, redA700, 4))
          }
        }
      }
    }
  }
}

function condiEntorno(cali, lugar, texto) {
  //Entorno
  if (cali < 10) {
    //LinearNulo()
    return (casosA(lugar, texto, cali, cyanA400, 0))
  } else {
    if (cali < 14) {
      //LinearBajo()
      return (casosA(lugar, texto, cali, lightGreenA400, 1))
    } else {
      if (cali < 18) {
        //LinearMedio()
        return (casosA(lugar, texto, cali, yellow500, 2))
      } else {
        if (cali < 23) {
          //LinearAlto()
          return (casosA(lugar, texto, cali, orange500, 3))
        } else {
          if (cali >= 23) {
            //MuyAlto
            return (casosA(lugar, texto, cali, redA700, 4))
          }
        }
      }
    }
  }
}

//Dominios
//Condiciones
function condiCondi(cali) {
  if (cali < 5) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali >= 5 && cali < 9) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali >= 9 && cali < 11) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali >= 11 && cali < 14) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 14) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Carga
function condiCarga(cali) {
  if (cali < 15) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 21) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 27) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 37) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 37) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Falta
function condiFalta(cali) {
  if (cali < 11) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 16) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 21) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 25) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 25) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Jornada
function condiJornada(cali) {
  if (cali < 1) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 2) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 4) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 6) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 6) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Interferencias
function condiInter(cali) {
  if (cali < 4) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 6) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 8) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 10) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 10) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Liderazgo
function condiLiderD(cali) {
  if (cali < 9) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 12) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 16) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 20) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 20) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Relaciones en el trabajo
function condiRelaciones(cali) {
  if (cali < 10) {
    //LinearNulo()
    return (casosA(13, "", cali, cyanA400, 0))
  } else {
    if (cali < 9) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 17) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 21) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 21) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Violencia
function condiViolencia(cali) {
  if (cali < 7) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 10) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 13) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 16) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 16) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Reconocimiento
function condiReconocimiento(cali) {
  if (cali < 6) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 10) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 14) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 18) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 18) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
//Insuficiente
function condiInsuficiente(cali) {
  if (cali < 4) {
    //LinearNulo()
    return (casosA(1, "", cali, cyanA400, 0))
  } else {
    if (cali < 6) {
      //LinearBajo()
      return (casosA(1, "", cali, lightGreenA400, 1))
    } else {
      if (cali < 8) {
        //LinearMedio()
        return (casosA(1, "", cali, yellow500, 2))
      } else {
        if (cali < 10) {
          //LinearAlto()
          return (casosA(1, "", cali, orange500, 3))
        } else {
          if (cali >= 10) {
            //MuyAlto
            return (casosA(1, "", cali, redA700, 4))
          }
        }
      }
    }
  }
}
