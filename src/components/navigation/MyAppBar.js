import React from 'react';
import AppBar from 'material-ui/AppBar';
import {indigo400} from 'material-ui/styles/colors';
import { Link } from 'react-router-dom';

export default class MyAppBar extends React.Component{
    render(){
        return(
            <Link to={"/"} style={{ textDecoration: 'none', color: 'black'}}>
            <AppBar
                title="Nom035"
                style={{'backgroundColor': indigo400}}
                
                //No mostrar el botón lateral, para mostrar usar TRUE
                showMenuIconButton={false}
            />
            </Link>
        );
    }
}