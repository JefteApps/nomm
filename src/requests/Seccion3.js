export default {
  Seccion3: [
    {
      "categoria" : "tiempo",
      "pregunta" : "Trabajo horas extras más de tres veces a la semana",
      "vrespuesta" : 14
    }, {
      "categoria" : "tiempo",
      "pregunta" : "Mi trabajo me exige laborar en días de descanso, festivos o fines de semana",
      "vrespuesta" : 15
    }, {
      "categoria" : "tiempo",
      "pregunta" : "Considero que el tiempo en el trabajo es mucho y perjudica mis actividades familiares o personales",
      "vrespuesta" : 16
    }, {
      "categoria" : "tiempo",
      "pregunta" : "Pienso en las actividades familiares o personales cuando estoy en mi trabajo",
      "vrespuesta" : 17
    }
  ]
}
