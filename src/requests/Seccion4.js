export default {
  Seccion4: [
    {
      "categoria" : "decisiones",
      "pregunta" : "Mi trabajo permite que desarrolle nuevas habilidades",
      "vrespuesta" : 18
    }, {
      "categoria" : "decisiones",
      "pregunta" : "en mi trabajo puedo aspirar a un mejor puesto",
      "vrespuesta" : 19
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Durante mi jornada de trabajo puedo tomar pausas cuando lo necesito",
      "vrespuesta" : 20
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Puedo decidir la velocidad a la que realizo mis actividades en mi trabajo",
      "vrespuesta" : 21
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Puedo cambiar el orden de las actividades que realizo en mi trabajo",
      "vrespuesta" : 22
    }
  ]
}
