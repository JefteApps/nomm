import React from 'react';

class ScrollTop extends React.Component {
  constructor(props) {
    super(props);
    this.resultsDiv = React.createRef();
  }

  someFunction(){
     this.resultsDiv.current.scrollTop = 0;
  }

  render() {
    return (
      <div ref={this.resultsDiv} />
    );
  }
}

export default ScrollTop;