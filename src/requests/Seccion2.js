export default {
  Seccion2: [
    {
      "categoria" : "actividades",
      "pregunta" : "En mi trabajo soy responsable de cosas de mucho valor",
      "vrespuesta" : 10
    }, {
      "categoria" : "actividades",
      "pregunta" : "Respondo ante mi jefe por los resultados de toda mi área de trabajo",
      "vrespuesta" : 11
    }, {
      "categoria" : "actividades",
      "pregunta" : "En mi trabajo me dan órdenes contradictorias",
      "vrespuesta" : 12
    }, {
      "categoria" : "actividades",
      "pregunta" : "Considero que en mi trabajo me piden hacer cosas innecesarias",
      "vrespuesta" : 13
    }
  ]
}
