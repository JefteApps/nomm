export default {
  Seccion5: [
    {
      "categoria" : "capacitación",
      "pregunta" : "Me informan con claridad cuáles son mis funciones",
      "vrespuesta" : 23
    }, {
      "categoria" : "capacitación",
      "pregunta" : "Me explican claramente los resultados que debo obtener en mi trabajo",
      "vrespuesta" : 24
    }, {
      "categoria" : "capacitación",
      "pregunta" : "Me informan con quién puedo resolver problemas o asuntos de trabajo",
      "vrespuesta" : 25
    }, {
      "categoria" : "capacitación",
      "pregunta" : "Me permiten asistir a capacitaciones relacionadas con mi trabajo",
      "vrespuesta" : 26
    }, {
      "categoria" : "capacitación",
      "pregunta" : "Recibo capacitación útil para hacer mi trabajo",
      "vrespuesta" : 27
    }
  ]
}
