export default {
  Seccion8: [
    {
      "categoria" : "atención",
      "pregunta" : "Para hacer mi trabajo debo demostrar sentimientos distintos a los míos",
      "vrespuesta" : 44
    }, {
      "categoria" : "atención",
      "pregunta" : "Soy jefe de otros trabajadores",
      "vrespuesta" : 45
    }, {
      "categoria" : "atención",
      "pregunta" : "Comunican tarde los asuntos de trabajo",
      "vrespuesta" : 46
    }, {
      "categoria" : "atención",
      "pregunta" : "Dificultan el logro de los resultados del trabajo",
      "vrespuesta" : 47
    }, {
      "categoria" : "atención",
      "pregunta" : "Ignoran las sugerencias para mejorar su trabajo",
      "vrespuesta" : 48
    }
  ]
}
