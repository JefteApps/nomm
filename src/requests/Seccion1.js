export default {
  Seccion1: [
    {
      "categoria" : "condiciones",
      "pregunta" : "Mi trabajo me exige hacer mucho esfuerzo físico",
      "vrespuesta" : 1
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Me preocupa sufrir un accidente en mi trabajo",
      "vrespuesta" : 2
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Considero que las actividades que realizo son peligrosas",
      "vrespuesta" : 3
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Por la cantidad de trabajo que tengo debo quedarme tiempo adicional a mi turno",
      "vrespuesta" : 4
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Por la cantidad de trabajo que tengo debo trabajar sin parar",
      "vrespuesta" : 5
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Considero que es necesario mantener un ritmo de trabajo acelerado",
      "vrespuesta" : 6
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Mi trabajo me exige que esté muy concentrado",
      "vrespuesta" : 7
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Mi trabajo requiere que memorice mucha información",
      "vrespuesta" : 8
    }, {
      "categoria" : "condiciones",
      "pregunta" : "Mi trabajo exige que atienda varios asuntos al mismo tiempo",
      "vrespuesta" : 9
    }
  ]
}
