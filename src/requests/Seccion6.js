export default {
  Seccion6: [
    {
      "categoria" : "relaciones",
      "pregunta" : "Mi jefe tiene en cuenta mis puntos de vista y opiniones",
      "vrespuesta" : 28
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Mi jefe ayuda a solucionar los problemas que se presentan en el trabajo",
      "vrespuesta" : 29
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Puedo confiar en mis compañeros de trabajo",
      "vrespuesta" : 30
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Cuando tenemos que realizar trabajo de equipo los compañeros colaboran",
      "vrespuesta" : 31
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Mis compañeros de trabajo me ayudan cuando tengo dificultades",
      "vrespuesta" : 32
    }, {
      "categoria" : "relaciones",
      "pregunta" : "En mi trabajo puedo expresarme libremente sin interrupciones",
      "vrespuesta" : 33
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Recibo críticas constantes a mi persona y/o trabajo",
      "vrespuesta" : 34
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Recibo burlas, calumnias, difamaciones, humillaciones o ridiculizaciones",
      "vrespuesta" : 35
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Se ignora mi persona o se me excluye de las reuniones de trabajo y en la toma de decisiones",
      "vrespuesta" : 36
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Se manipulas las situaciones de trabajo para hacerme parecer un mal trabajador",
      "vrespuesta" : 37
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Se ignoran mis éxitos laborales y se atribuyen a otros trabajadores",
      "vrespuesta" : 38
    }, {
      "categoria" : "relaciones",
      "pregunta" : "Me bloquean o impiden las oportunidades que tengo para obtener ascenso o mejorar en mi trabajo",
      "vrespuesta" : 39
    }, {
      "categoria" : "relaciones",
      "pregunta" : "He presenciado actos de violencia en mi centro de trabajo",
      "vrespuesta" : 40
    }
  ]
}
