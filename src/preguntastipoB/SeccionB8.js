export default {
  Seccion1: [
    {
      "categoria" : "capacitacion",
      "pregunta" : "Me informan con claridad cuáles son mis funciones",
      "vrespuesta" : 31
    }, {
      "categoria" : "capacitacion",
      "pregunta" : "Me explican claramente los resultados que debo obtener en mi trabajo",
      "vrespuesta" : 32
    }, {
      "categoria" : "capacitacion",
      "pregunta" : "Me explican claramente los objetivos de mi trabajo",
      "vrespuesta" : 33
    }, {
      "categoria" : "capacitacion",
      "pregunta" : "Me informan con quién puedo resolver problemas o asuntos de trabajo",
      "vrespuesta" : 34
    }, {
      "categoria" : "capacitacion",
      "pregunta" : "Me permiten asistir a capacitaciones relacionadas con mi trabajo",
      "vrespuesta" : 35
    }, {
      "categoria" : "capacitacion",
      "pregunta" : "Recibo capacitación útil para hacer mi trabajo",
      "vrespuesta" : 36
    } ]
}
