export default {
  Seccion1: [
    {
      "categoria" : "esfuerzo",
      "pregunta" : "Mi trabajo exige que esté muy concentrado",
      "vrespuesta" : 9
    }, {
      "categoria" : "esfuerzo",
      "pregunta" : "Mi trabajo requiere que memorice mucha información",
      "vrespuesta" : 10
    }, {
      "categoria" : "esfuerzo",
      "pregunta" : "En mi trabajo tengo que tomar decisiones difíciles muy rápido",
      "vrespuesta" : 11
    }, {
      "categoria" : "esfuerzo",
      "pregunta" : "Mi trabajo exige que atienda varios asuntos al mismo tiempo",
      "vrespuesta" : 12
    }]
}
