export default {
  Seccion1: [
    {
      "categoria" : "atencion",
      "pregunta" : "Atiendo clientes o usuarios muy enojados",
      "vrespuesta" : 65
    }, {
      "categoria" : "atencion",
      "pregunta" : "Mi trabajo me exige atender personas muy necesitadas de ayuda o enfermas",
      "vrespuesta" : 66
    }, {
      "categoria" : "atencion",
      "pregunta" : "Para hacer mi trabajo debo demostrar sentimientos distintos a los míos",
      "vrespuesta" : 67
    }, {
      "categoria" : "atencion",
      "pregunta" : "Mi trabajo me exige atender situaciones de violencia",
      "vrespuesta" : 68
    } ]
}