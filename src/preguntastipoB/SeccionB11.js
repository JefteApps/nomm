export default {
  Seccion1: [
  {
      "categoria" : "rendimiento",
      "pregunta" : "Me informan sobre lo que hago bien en mi trabajo",
      "vrespuesta" : 47
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "La forma como evalúan mi trabajo en mi centro de trabajo me ayuda a mejorar mi desempeño",
      "vrespuesta" : 48
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "En mi centro de trabajo me pagan a tiempo mi salario",
      "vrespuesta" : 49
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "El pago que recibo es el que merezco por el trabajo que realizo",
      "vrespuesta" : 50
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "Si obtengo los resultados esperados en mi trabajo me recompensan o reconocen",
      "vrespuesta" : 51
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "Las personas que hacen bien el trabajo pueden crecer laboralmente",
      "vrespuesta" : 52
    }, {
      "categoria" : "rendimiento",
      "pregunta" : "Considero que mi trabajo es estable",
      "vrespuesta" : 53
    }]
}
