export default {
  Seccion1: [
   {
      "categoria" : "decisiones",
      "pregunta" : "Mi trabajo permite que desarrolle nuevas habilidades",
      "vrespuesta" : 23
    }, {
      "categoria" : "decisiones",
      "pregunta" : "En mi trabajo puedo aspirar a un mejor puesto",
      "vrespuesta" : 24
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Durante mi jornada de trabajo puedo tomar pausas cuando las necesito",
      "vrespuesta" : 25
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Puedo decidir cuánto trabajo realizo durante la jornada laboral",
      "vrespuesta" : 26
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Puedo decidir la velocidad a la que realizo mis actividades en mi trabajo",
      "vrespuesta" : 27
    }, {
      "categoria" : "decisiones",
      "pregunta" : "Puedo cambiar el orden de las actividades que realizo en mi trabajo",
      "vrespuesta" : 28
    } ]
}
