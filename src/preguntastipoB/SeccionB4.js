export default {
  Seccion1: [
    {
      "categoria" : "responsabilidades",
      "pregunta" : "En mi trabajo soy responsable de cosas de mucho valor",
      "vrespuesta" : 13
    }, {
      "categoria" : "responsabilidades",
      "pregunta" : "Respondo ante mi jefe por los resultados de toda mi área de trabajo",
      "vrespuesta" : 14
    }, {
      "categoria" : "responsabilidades",
      "pregunta" : "En el trabajo me dan órdenes contradictorias",
      "vrespuesta" : 15
    }, {
      "categoria" : "responsabilidades",
      "pregunta" : "Considero que en mi trabajo me piden hacer cosas innecesarias",
      "vrespuesta" : 16
    }]
}
