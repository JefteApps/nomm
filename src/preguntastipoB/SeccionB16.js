export default {
  Seccion1: [
    {
      "categoria" : "atencion",
      "pregunta" : "Comunican tarde los asuntos de trabajo",
      "vrespuesta" : 69
    }, {
      "categoria" : "atencion",
      "pregunta" : "Dificultan el logro de los resultados del trabajo",
      "vrespuesta" : 70
    }, {
      "categoria" : "atencion",
      "pregunta" : "Cooperan poco cuando se necesita",
      "vrespuesta" : 71
    }, {
      "categoria" : "atencion",
      "pregunta" : "Ignoran las sugerencias para mejorar su trabajo",
      "vrespuesta" : 72
    } ]
}