export default {
  Seccion1: [
    {
      "categoria" : "jefes",
      "pregunta" : "Mi jefe ayuda a organizar mejor el trabajo",
      "vrespuesta" : 37
    }, {
      "categoria" : "jefes",
      "pregunta" : "Mi jefe tiene en cuenta mis puntos de vista y opiniones",
      "vrespuesta" : 38
    }, {
      "categoria" : "jefes",
      "pregunta" : "Mi jefe me comunica a tiempo la información relacionada con el trabajo",
      "vrespuesta" : 39
    }, {
      "categoria" : "jefes",
      "pregunta" : "La orientación que me da mi jefe me ayuda a realizar mejor mi trabajo",
      "vrespuesta" : 40
    }, {
      "categoria" : "jefes",
      "pregunta" : "Mi jefe ayuda a solucionar los problemas que se presentan en el trabajo",
      "vrespuesta" : 41
    } ]
}
