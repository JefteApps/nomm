export default {
  Seccion1: [
    {
      "categoria" : "jornada",
      "pregunta" : "Trabajo horas extras más de tres veces a la semana",
      "vrespuesta" : 17
    }, {
      "categoria" : "jornada",
      "pregunta" : "Mi trabajo me exige laborar en días de descanso, festivos o fines de semana",
      "vrespuesta" : 18
    }, {
      "categoria" : "jornada",
      "pregunta" : "Considero que el tiempo en el trabajo es mucho y perjudica mis actividades familiares o personales",
      "vrespuesta" : 19
    }, {
      "categoria" : "jornada",
      "pregunta" : "Debo atender asuntos de trabajo cuando estoy en casa",
      "vrespuesta" : 20
    }, {
      "categoria" : "jornada",
      "pregunta" : "Pienso en las actividades familiares o personales cuando estoy en mi trabajo",
      "vrespuesta" : 21
    }, {
      "categoria" : "jornada",
      "pregunta" : "Pienso que mis responsabilidades familiares afectan mi trabajo",
      "vrespuesta" : 22
    }]
}
