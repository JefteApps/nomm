export default {
  Seccion1: [
    {
      "categoria" : "ritmo",
      "pregunta" : "Por la cantidad de trabajo que tengo debo quedarme tiempo adicional a mi turno",
      "vrespuesta" : 6
    }, {
      "categoria" : "ritmo",
      "pregunta" : "Por la cantidad de trabajo que tengo debo trabajar sin parar",
      "vrespuesta" : 7
    }, {
      "categoria" : "ritmo",
      "pregunta" : "Considero que es necesario mantener un ritmo de trabajo acelerado",
      "vrespuesta" : 8
    } ]
}
