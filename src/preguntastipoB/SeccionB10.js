export default {
  Seccion1: [
   {
      "categoria" : "compañeros",
      "pregunta" : "Puedo confiar en mis compañeros de trabajo",
      "vrespuesta" : 42
    }, {
      "categoria" : "compañeros",
      "pregunta" : "Entre compañeros solucionamos los problemas de trabajo de forma respetuosa",
      "vrespuesta" : 43
    }, {
      "categoria" : "compañeros",
      "pregunta" : "En mi trabajo me hacen sentir parte del grupo",
      "vrespuesta" : 44
    }, {
      "categoria" : "compañeros",
      "pregunta" : "Cuando tenemos que realizar trabajo de equipo los compañeros colaboran",
      "vrespuesta" : 45
    }, {
      "categoria" : "compañeros",
      "pregunta" : "Mis compañeros de trabajo me ayudan cuando tengo dificultades",
      "vrespuesta" : 46
    } ]
}
