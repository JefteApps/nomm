export default {
  Seccion1: [
    {
      "categoria" : "violencia",
      "pregunta" : "Recibo críticas constantes a mi persona y/o trabajo",
      "vrespuesta" : 58
    }, {
      "categoria" : "violencia",
      "pregunta" : "Recibo burlas, calumnias, difamaciones, humillaciones o ridiculizaciones",
      "vrespuesta" : 59
    }, {
      "categoria" : "violencia",
      "pregunta" : "Se ignora mi presencia o se me excluye de las reuniones de trabajo y en la toma de decisiones",
      "vrespuesta" : 60
    }, {
      "categoria" : "violencia",
      "pregunta" : "Se manipulan las situaciones de trabajo para hacerme parecer un mal trabajador",
      "vrespuesta" : 61
    }, {
      "categoria" : "violencia",
      "pregunta" : "Se ignoran mis éxitos laborales y se atribuyen a otros trabajadores",
      "vrespuesta" : 62
    }, {
      "categoria" : "violencia",
      "pregunta" : "Me bloquean o impiden las oportunidades que tengo para obtener ascenso o mejora en mi trabajo",
      "vrespuesta" : 63
    }, {
      "categoria" : "violencia",
      "pregunta" : "He presenciado actos de violencia en mi centro de trabajo",
      "vrespuesta" : 64
    } ]
}
