import React from 'react';
import Title from '../components/Title';
import TextField from '@material-ui/core/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Conteiner from '../components/Container';
import firebase from '../Firebase/Firebase';
import { ListItem } from 'material-ui';
import { makeStyles, useTheme, withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';

import FormControl from '@material-ui/core/FormControl';

import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Autocomplete from '@material-ui/lab/Autocomplete';

var l = "", c = ""
var tipoEncuesta = ""
var varEmp = "", varClave = ""
//var mValor =""
var elValor = ""
var cantDepa = ""
var cantClave = ""
var arrClaves = []
var miLogin = false
var arrDepa = ["Esto es una", "prueba de listas", "para ver si lo pone"]
var setPersonName = ""
var options = ['Create a merge commit', 'Squash and merge', 'Rebase and merge'];
var htz = ""



const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
}));
const AntSwitch = withStyles(theme => ({
    root: {
        width: 28,
        height: 16,
        padding: 0,
        display: 'flex',
    },
    switchBase: {
        padding: 2,
        color: theme.palette.grey[500],
        '&$checked': {
            transform: 'translateX(12px)',
            color: theme.palette.common.white,
            '& + $track': {
                opacity: 1,
                backgroundColor: theme.palette.primary.main,
                borderColor: theme.palette.primary.main,
            },
        },
    },
    thumb: {
        width: 12,
        height: 12,
        boxShadow: 'none',
    },
    track: {
        border: `1px solid ${theme.palette.grey[500]}`,
        borderRadius: 16 / 2,
        opacity: 1,
        backgroundColor: theme.palette.common.white,
    },
    checked: {},
}))(Switch);
const classes = useStyles;

//const [personName, setPersonName] = React.useState([]);
export default class MenuGrafica extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clave: "",
            correo: "",
            empresaID: "",
            tEcuesta: "",
            departamento: "",
            comClave: "",
            comValor: "",
            arrDepaST: [],
            arrFolio: [],
            personName: "",
            setPersonName: "Nada",
            setFolio: "Nada",
            loginAdmin: false,
            checkedA: true,
            slc: ""
        }
    }
    componentDidMount() {
        l = sessionStorage.getItem('login')
        c = sessionStorage.getItem('correo')
        //alert("variable local es "+l)
        if (l != null) {
            // alert("Login con LOCAL")
            this.mostrarEncuesta(l, c)
        }
    }

    componentWillMount() {
        //const { idCorreo } = this.props.location.state
        //htz=idCorreo
        //alert("mi correo es "+idCorreo)
        const handleChangeMultiple = event => {
            const { options } = event.target;
            const value = [];
            for (let i = 0, l = options.length; i < l; i += 1) {
                if (options[i].selected) {
                    value.push(options[i].value);
                }
            }
            this.setState({ setPersonName: value })
            this.setState({ setFolio: value })
        };


    }
    mostrarEncuesta(claveS, correo) {

        var claved = claveS;

        varEmp = claved.substring(0, 3)
        //alert("Empresa "+varEmp)
        tipoEncuesta = claved.substring(3, 4)
        //alert("Tipo de encuesta "+tipoEncuesta)
        //var varDepa=claved.substring(4,5)
        //alert("Depa "+varDepa)
        varClave = claved.substring(4)
        //alert("clave "+varClave)
        this.setState({
            empresaID: varEmp,
            tEcuesta: tipoEncuesta,
            comClave: varClave
        })
        try {
            const nameRef = firebase.database().ref()
                .child('auth/' + claved)
                .child('correo')
            //https://nom-035.firebaseio.com/JAX/B/1/clave/001/valor
            //const nameRef =firebase.database().ref('https://nom-035.firebaseio.com/'+varEmp+'/'+tipoEncuesta+'/'+varDepa+'/clave/'+varClave+'/valor')
            elValor = nameRef
            elValor.on('value', function (snapshot) {
                if (snapshot.val() === null) {
                    alert("Contraseña no valida, verifique")
                } else {
                    if (snapshot.val() === correo) {

                        sessionStorage.setItem('login', claveS)
                        sessionStorage.setItem('correo', correo)
                        document.getElementById("MenuID").style.display = 'block';
                        document.getElementById("mclave").style.display = 'none';
                    } else {
                        alert("Usuario no valido, verifique")
                    }

                }
            });
            const ref = firebase.database().ref().child('cliente').child('id').child(claved.substring(0, 3))
            try {
                ref.on('value', (data) => {
                    try {
                        cantDepa = data.val().cantDepa
                    } catch (error) {
                        console.error(error);
                        cantDepa = 0
                        // expected output: ReferenceError: nonExistentFunction is not defined
                        // Note - error messages will vary depending on browser
                    }

                    for (var i = 0; i < cantDepa; i++) {
                        ref.child('depa').child(i).on('value', (depa) => {

                            arrDepa[i] = depa.val().nombre
                        })
                    }
                    this.setState({
                        arrDepaST: arrDepa
                    })

                })
            } catch (error) {
                alert("Error en la clave")
                console.error(error);
                // expected output: ReferenceError: nonExistentFunction is not defined
                // Note - error messages will vary depending on browser
            }

            ref.on('value', (data) => {
                try {
                    cantClave = data.val().cant
                } catch (error) {
                    console.error(error);
                    cantClave = 0
                    // expected output: ReferenceError: nonExistentFunction is not defined
                    // Note - error messages will vary depending on browser
                }
                for (var i = 0; i < cantClave; i++) {
                    ref.child('claves').child(i).on('value', (folio) => {
                        arrClaves[i] = folio.val()
                    })
                }
                this.setState({
                    arrFolio: arrClaves

                })

            })

        }
        catch (error) {
            alert("Error en la clave")
            console.error(error);
            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    }



    render() {
        return (
            <section>
                <div className="row middle-xs" id="mclave">
                    <div className="col-xs-12 col-sm-6">
                        <Conteiner>
                            <Title />
                            <form>
                                <TextField
                                    id="correo"
                                    label="Usuario"
                                    type="search"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.correo}
                                    type="email"
                                    onChange={e => this.setState({ correo: e.target.value })}
                                />
                                <TextField
                                    id="clave"
                                    label="Contraseña"
                                    type="search"
                                    margin="normal"
                                    variant="outlined"
                                    type="password"
                                    value={this.state.clave}
                                    onChange={e => this.setState({ clave: e.target.value })}
                                />
                            </form>

                            <div className="Login-actions">
                                <RaisedButton
                                    label="INICIAR"
                                    secondary={true}
                                    onClick={e => (this.mostrarEncuesta(this.state.clave, this.state.correo))}
                                />
                            </div>
                        </Conteiner>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <div className="login-Background"
                            style={{
                                'backgroundImage':
                                    "url(" +
                                    process.env.PUBLIC_URL + '/images/nomclave.jpg' + ")"
                            }}>
                        </div>
                    </div>
                </div>
                <section id="eA" style={{ 'display': 'none' }}>
                </section>
                {
                    //Primera ventana despues del login
                }

                <section id="MenuID" style={{ 'display': 'none'}}>
                  <div className="row">
                    <div className="col-xs-12 col-sm-6">
                      <div className="login-Background"
                          style={{
                              'backgroundImage':
                                  "url(" +
                                  process.env.PUBLIC_URL + '/images/nommenu.jpg' + ")"
                          }}>
                      </div>
                    </div>
                    <div className="col-xs-12 col-sm-6" style={{'textAlign':'center'}}>
                      <h4>¿Qué resultados quieres ver?</h4>
                      <Grid item>
                          <ButtonGroup
                              color="secondary"
                              size="large"
                              aria-label="large outlined secondary button group">
                              <Button onClick={e => { mostrarOcultar(1) }}>General</Button>
                              <Button onClick={e => { mostrarOcultar(2) }}>Departamento</Button>
                              <Button style={{display:'none'}} onClick={e => { mostrarOcultar(3) }}>Folio</Button>
                              <Link to={{ pathname: '/' }}
                                  style={{ textDecoration: 'none', color: 'black' }}>
                                  <RaisedButton style={{ marginTop: '5px', marginLeft: '30px',
                                    'textAlign':'right'}}
                                      label="Cerrar sesión"
                                      secondary={true}
                                      onClick={e => (sessionStorage.clear(), window.location.reload())}
                                  />
                              </Link>
                          </ButtonGroup>
                      </Grid>
                      <section id="slcDepa" style={{ 'display': 'none', marginTop: '60px', marginRight: '30px',
                      'textAlign': 'center' }}>
                          <h4>Por favor seleccione un departamento</h4>

                          <FormControl className={classes.formControl}>
                              <InputLabel shrink htmlFor="select-multiple-native">
                                  Departamentos
                              </InputLabel>
                              <Select
                                  multiple
                                  native
                                  value={this.state.setPersonName}
                                  onChange={e => this.setState({ setPersonName: e.target.value })}
                                  inputProps={{
                                      id: 'select-multiple-native',
                                  }}
                              >
                                  {this.state.arrDepaST.map(name => (
                                      <option key={name} value={name}>
                                          {name}
                                      </option>
                                  ))}
                              </Select>
                          </FormControl>
                          <ListItem >
                              Se consultara el departamento: .
                              {this.state.setPersonName}
                              {verificarDepa(this.state.setPersonName)}
                          </ListItem>

                          <Link id="btnConsultarDepa" to={{
                              pathname: '/Departamento', state: {
                                  idLugar: "2",
                                  idEmpresa: this.state.empresaID,
                                  idEncuesta: this.state.tEcuesta,
                                  idDepa: this.state.setPersonName
                              }
                          }}
                              style={{ 'display': 'none', textDecoration: 'none', color: 'black' }}>
                              <RaisedButton
                                  label="Consultar"
                                  secondary={true}
                              //onClick={e => (this.mostrarEncuesta(2))}
                              />
                          </Link>

                      </section>

                      <section id="idGeneral" style={{ 'display': 'none', 'textAlign':'center',
                        marginRight:'30px'}}>
                          <Link to={{
                              pathname: '/General', state: {
                                  idLugar: "1",
                                  idEmpresa: this.state.empresaID,
                                  idEncuesta: this.state.tEcuesta
                              }
                          }}
                              style={{ textDecoration: 'none', color: 'black' }}>
                              <RaisedButton style={{ marginTop: '30px', marginLeft: '30px' }}
                                  label="Consultar"
                                  secondary={true}
                              //onClick={e => (this.mostrarEncuesta(2))}
                              />
                          </Link>
                      </section>
                    </div>
                  </div>
                </section>

                {
                    //Folio
                }
                <section id="slcFolio" style={{ 'display': 'none', marginTop: '60px', marginRight: '30px',
                  'textAlign':'center'}}>
                    <h4>Por favor seleccione un Folio</h4>
                    <FormControl className={classes.formControl}>
                        <InputLabel shrink htmlFor="select-multiple-native">
                            Folio
                        </InputLabel>
                        <Select
                            multiple
                            native
                            value={this.state.setFolio}
                            onChange={e => this.setState({ setFolio: e.target.value })}
                            inputProps={{
                                id: 'select-multiple-native',
                            }}
                        >
                            {this.state.arrFolio.map(name => (
                                <option key={name} value={name}>
                                    {name}
                                </option>
                            ))}
                        </Select>
                    </FormControl>
                    <ListItem >
                        Se consultara el folio: .
                        {this.state.setFolio}
                        {verificarFolio(this.state.setFolio)}
                    </ListItem>
                    <Link id="btnConsultarFolio" to={{
                        pathname: '/Folio', state: {
                            idLugar: "3",
                            idEmpresa: this.state.empresaID,
                            idEncuesta: this.state.tEcuesta,
                            idClave: this.state.setFolio
                        }
                    }}

                        style={{ 'display': 'none', textDecoration: 'none', color: 'black' }}>
                        <RaisedButton
                            label="Consultar"
                            secondary={true}
                        //onClick={e => (this.mostrarEncuesta(2))}
                        />
                    </Link>
                </section>
            </section>
        )
    }
}
function verificarDepa(depaSelect) {
    if (depaSelect == "Nada") {

    } else {
        document.getElementById("btnConsultarDepa").style.display = 'block'
    }
}
function verificarFolio(depaSelect) {
    if (depaSelect == "Nada") {

    } else {
        document.getElementById("btnConsultarFolio").style.display = 'block'
    }
}
function mostrarOcultar(valor) {
    switch (valor) {
        case 1:
            document.getElementById("idGeneral").style.display = 'block'
            document.getElementById("slcDepa").style.display = 'none';
            document.getElementById("slcFolio").style.display = 'none';
            break
        case 2:
            document.getElementById("slcDepa").style.display = 'block';
            document.getElementById("slcFolio").style.display = 'none';
            document.getElementById("idGeneral").style.display = 'none';
            break
        case 3:
            document.getElementById("slcFolio").style.display = 'block';
            document.getElementById("slcDepa").style.display = 'none';
            document.getElementById("idGeneral").style.display = 'none';
            break
    }
}
