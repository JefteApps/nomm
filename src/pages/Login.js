import React from 'react';
import Title from '../components/Title';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Conteiner from '../components/Container';
import { Link } from 'react-router-dom';
import firebase from '../Firebase/Firebase';

var email = ""

export default class Login extends React.Component {
    constructor(props) {
        super(props)
        this.autentificar = this.autentificar.bind(this)
        this.state = {
            miEmail: "uno"
        }
    }
    autentificar() {
        this.setState({
            miEmail:this.refs.textEmail.getValue()
        })
        email = this.refs.textEmail.getValue()

        var password = this.refs.textPassword.getValue()
        firebase.auth().signInWithEmailAndPassword(email, password).then(function () {
            // The link was successfully sent. Inform the user.
            // Save the email locally so you don't need to ask the user for it again
            // if they open the link on the same device.
            verificado()
        }).catch(function (error) {
            // Handle Errors here.

            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
            alert("Error al ingresar: " + errorCode)
        });
    }
    MiAuth() {
        var email = this.refs.textEmail.getValue()
        var password = this.refs.textPassword.getValue()
        firebase.database().ref().child(password).once('value', (snapshot) => {
            if (snapshot.val().correo === email) {
                return (true)
            } else {
                return (false)
            }
        })
    }
    render() {
        return (
            <div className="row middle-xs">
                <div className="col-xs-12 col-sm-6">
                    <Conteiner>
                        <Title />
                        <TextField
                            className="textfield"
                            floatingLabelText="Correo electrónico"
                            type="email"
                            ref="textEmail"
                        />
                        <TextField
                            className="textfield"
                            floatingLabelText="Contraseña"
                            type="password"
                            ref="textPassword"
                        />
                        <div className="Login-actions">
                            {//<Link to="/signup" style ={{"marginRight":"1em"}}>Crear nueva cuenta</Link>
                            }
                            <RaisedButton
                                label="Ingresar"
                                secondary={true}
                                onClick={this.autentificar}
                            />
                            <Link id="idConsul" to={{ pathname: '/', state: { idCorreo: this.state.miEmail} }}
                                style={{ 'display': 'none' }}>
                                <RaisedButton
                                    label="Consultar"
                                    secondary={true}
                                />
                            </Link>
                        </div>
                    </Conteiner>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <div className="login-Background"
                        style={{
                            'backgroundImage':
                                "url(" +
                                process.env.PUBLIC_URL + '/images/login-background.jpg' + ")"
                        }}>

                    </div>
                </div>
            </div>
        )
    }
}
function verificado() {
    //alert("Login fue posible")
    document.getElementById("idConsul").style.display = 'block';
}