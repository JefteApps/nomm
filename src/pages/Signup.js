import React from 'react';
import Title from '../components/Title';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import RadioButton from 'material-ui/RadioButton';
import {Link} from 'react-router-dom';

import Conteiner from '../components/Container';

export default class Signup extends React.Component{
    render(){
        return(
            <div className="row middle-xs">
                <div className="col-xs-12 col-sm-6">
                    <Conteiner>
                        <Title/>
                        <TextField 
                            className="textfield"
                            floatingLabelText="Correo electrónico"
                            type="email"
                        />
                        <TextField 
                            className="textfield"
                            floatingLabelText="Contraseña"
                            type="password"
                        />
                        <div className="Login-actions">
                            <Link to="/login" style ={{"marginRight":"1em"}}>Ya tengo cuenta</Link>
                            <RaisedButton
                                label="Crear cuenta"
                                secondary={true}
                            />
                            
                        </div>
                    </Conteiner>
                </div>
                <div className="col-xs-12 col-sm-6">
                    <div className="login-Background" 
                    style={{'backgroundImage':
                    "url("+
                    process.env.PUBLIC_URL + '/images/singup.jpg'+")"}}>

                    </div>
                </div>
            </div>
        )
    }
}