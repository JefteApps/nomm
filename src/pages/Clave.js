import React from 'react';
import Title from '../components/Title';
import TextField from '@material-ui/core/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Conteiner from '../components/Container';
import EncuestaB from '../components/encuestas/EncuestaB';
import EncuestaA from '../components/encuestas/EncuestaA';
import firebase from '../Firebase/Firebase';
import { ListItem } from 'material-ui';
import { makeStyles ,withStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';

import FormControl from '@material-ui/core/FormControl';

import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';



var tipoEncuesta = ""
var varEmp = "", varClave = ""
//var mValor =""
var elValor = ""
var cantDepa = ""
var arrDepa = ["Esto es una", "prueba de listas", "para ver si lo pone"]


const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
        maxWidth: 300,
    },
    chips: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    chip: {
        margin: 2,
    },
    noLabel: {
        marginTop: theme.spacing(3),
    },
}));
const AntSwitch = withStyles(theme => ({
    root: {
      width: 28,
      height: 16,
      padding: 0,
      display: 'flex',
    },
    switchBase: {
      padding: 2,
      color: theme.palette.grey[500],
      '&$checked': {
        transform: 'translateX(12px)',
        color: theme.palette.common.white,
        '& + $track': {
          opacity: 1,
          backgroundColor: theme.palette.primary.main,
          borderColor: theme.palette.primary.main,
        },
      },
    },
    thumb: {
      width: 12,
      height: 12,
      boxShadow: 'none',
    },
    track: {
      border: `1px solid ${theme.palette.grey[500]}`,
      borderRadius: 16 / 2,
      opacity: 1,
      backgroundColor: theme.palette.common.white,
    },
    checked: {},
  }))(Switch);
const classes = useStyles;

//const [personName, setPersonName] = React.useState([]);
export default class Clave extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clave: "",
            empresaID: "",
            tEcuesta: "",
            departamento: "",
            comClave: "",
            comValor: "",
            arrDepaST: [],
            personName: "",
            setPersonName: "Nada",
            checkedC: true,
            checkedA: true
        }
    }
    componentWillMount() {
        const handleChangeMultiple = event => {
            const { options } = event.target;
            const value = [];
            for (let i = 0, l = options.length; i < l; i += 1) {
                if (options[i].selected) {
                    value.push(options[i].value);
                }
            }
            this.setState({ setPersonName: value })
        };
    }
    mostrarEncuesta(lugar) {
        var claved = this.state.clave;

        varEmp = claved.substring(0, 3)
        //alert("Empresa "+varEmp)
        tipoEncuesta = claved.substring(3, 4)
        //alert("Tipo de encuesta "+tipoEncuesta)
        //var varDepa=claved.substring(4,5)
        //alert("Depa "+varDepa)
        varClave = claved.substring(4)
        //alert("clave "+varClave)
        this.setState({
            empresaID: varEmp,
            tEcuesta: tipoEncuesta,
            comClave: varClave
        })
        try {
            const nameRef = firebase.database().ref()
                .child('empresa/' + claved.substring(0, 3))
                .child('' + claved.substring(3, 4))
                .child('clave')
                .child('' + claved.substring(4))
                .child('valor')
            //https://nom-035.firebaseio.com/JAX/B/1/clave/001/valor
            //const nameRef =firebase.database().ref('https://nom-035.firebaseio.com/'+varEmp+'/'+tipoEncuesta+'/'+varDepa+'/clave/'+varClave+'/valor')
            elValor = nameRef
            elValor.on('value', function (snapshot) {
                varli(snapshot.val(), lugar)
            });

            const ref = firebase.database().ref().child('cliente').child('id').child(claved.substring(0, 3))
            ref.on('value', (data) => {
                cantDepa = data.val().cantDepa
                for (var i = 0; i < cantDepa; i++) {
                    ref.child('depa').child(i).on('value', (depa) => {
                        arrDepa[i] = depa.val().nombre
                    })
                }
                this.setState({
                    arrDepaST: arrDepa
                })

            })

        }
        catch (error) {
            alert("Error en la clave")
            console.error(error);
            // expected output: ReferenceError: nonExistentFunction is not defined
            // Note - error messages will vary depending on browser
        }

    }
    render() {
        return (
            <section>
                <div className="row middle-xs" id="mclave">
                    <div className="col-xs-12 col-sm-6">
                        <Conteiner>
                            <Title />
                            <form>
                                <TextField
                                    id="clave"
                                    label="Introduce la Clave"
                                    type="search"
                                    margin="normal"
                                    variant="outlined"
                                    value={this.state.clave}
                                    onChange={e => this.setState({ clave: e.target.value })}
                                />
                            </form>
                            <div className="Login-actions">
                                <RaisedButton
                                    label="INICIAR"
                                    secondary={true}
                                    onClick={e => (this.mostrarEncuesta(1))}
                                />
                            </div>
                        </Conteiner>
                    </div>
                    <div className="col-xs-12 col-sm-6">
                        <div className="login-Background"
                            style={{
                                'backgroundImage':
                                    "url(" +
                                    process.env.PUBLIC_URL + '/images/nomclave.jpg' + ")"
                            }}>
                        </div>
                    </div>
                </div>
                <section id="eA" style={{ 'display': 'none' }}>
                </section>


                <section id="listDepa" style={{ 'display': 'none', marginTop: '60px', marginLeft: '30px'}}>
                    <h4>Por favor seleccione un departamento al cual pertenece o se adecúe mas</h4>
                    <FormControl className={classes.formControl}>
                        <InputLabel shrink htmlFor="select-multiple-native">
                            Departamentos
                        </InputLabel>
                        <Select
                            multiple
                            native
                            value={this.state.setPersonName}
                            onChange={e => this.setState({ setPersonName: e.target.value })}
                            inputProps={{
                                id: 'select-multiple-native',
                            }}
                        >
                            {this.state.arrDepaST.map(name => (
                                <option key={name} value={name}>
                                    {name}
                                </option>
                            ))}
                        </Select>
                    </FormControl>
                    <ListItem >
                        A seleccionado: 
                        {this.state.setPersonName}
                    </ListItem>
                    <h4>Marcar SI o NO segun sea el caso</h4>
                    <p>En mi trabajo debo brindar servicio a clientes o usuarios</p>
                    <Typography component="div">
                        <Grid component="label" container alignItems="center" spacing={1}>
                            <Grid item>No</Grid>
                            <Grid item>
                                <AntSwitch
                                    checked={this.state.checkedC}
                                    onChange={e => (this.setState({checkedC: e.target.checked}),alert(this.state.checkedC))}
                                    value="checkedC"
                                />
                            </Grid>
                            <Grid item>Si</Grid>
                        </Grid>
                    </Typography>
                    <p>Soy jefe de otros trabajadores:</p>
                    <Typography component="div">
                        <Grid component="label" container alignItems="center" spacing={1}>
                            <Grid item>No</Grid>
                            <Grid item>
                                <AntSwitch
                                    checked={this.state.checkedA}
                                    onChange={e => (this.setState({checkedA: e.target.checkeA}),alert(this.state.checkedA))}
                                    value="checkedA"
                                />
                            </Grid>
                            <Grid item>Si</Grid>
                        </Grid>
                    </Typography>
                    <RaisedButton
                        label="INICIAR"
                        secondary={true}
                        onClick={e => (this.mostrarEncuesta(2))}
                    />
                </section>
                <section id="eB" style={{ 'display': 'none' }}>
                    <EncuestaB
                        idEmpresa={this.state.empresaID}
                        idEncuesta={this.state.tEcuesta}
                        idClave={this.state.comClave}
                        checkServi={this.state.checkedC}
                        checkJefe={this.state.checkedA}
                        departamento={this.state.setPersonName}>
                            
                    </EncuestaB>
                </section>
            </section>
        )
    }
}
function varli(snapshot, lugar) {
    if (lugar == 1) {
        if (snapshot == "2") {
            alert("Ya se realizo una ecuesta con esta clave")
        } else {
            //alert("En el else "+snapshot)
            if (snapshot === null) {
                alert("Clave no valida, verifique")
            } else {
                document.getElementById("listDepa").style.display = 'block';
                document.getElementById("mclave").style.display = 'none';
            }
        }
    }
    else {
        if (snapshot == "2") {
            alert("Ya se realizo una ecuesta con esta clave")
        } else {
            //alert("En el else "+snapshot)
            if (snapshot === null) {
                alert("Clave no valida, verifique")
            } else {
                // document.getElementById("listDepa").style.display = 'block';

                if (tipoEncuesta == "B") {
                    //alert("Encuesta tipo B")
                    document.getElementById("eB").style.display = 'block';
                    document.getElementById("listDepa").style.display = 'none';

                } else {
                    if (tipoEncuesta == "A") {
                        //alert("Encuesta tipo A")
                        document.getElementById("eA").style.display = 'block';
                        document.getElementById("listDepa").style.display = 'none';
                    } else {
                        alert("No se detecto el tipo de encuesta")
                    }
                }
            }
        }
    }
    //alert(snapshot)

}

