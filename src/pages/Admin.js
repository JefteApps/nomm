import React from 'react';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import NumberFormat from 'react-number-format';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import firebase from '../Firebase/Firebase';
import {
    fade,
    ThemeProvider,
    withStyles,
    makeStyles,
    createMuiTheme,
} from '@material-ui/core/styles';


var arrTodo = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
    , "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
var arrMiClave = []
var arrMostrar = []
var tipoEncuesta = "A"
var empresaID = ""
var arrDepaFB = []
var arrDepaFBPro = []
var nombreE = ""
var cantEn = ""

const CssTextField = withStyles({
    root: {
        '& label.Mui-focused': {
            color: 'green',
        },
        '& .MuiInput-underline:after': {
            borderBottomColor: 'green',
        },
        '& .MuiOutlinedInput-root': {
            '& fieldset': {
                borderColor: 'red',
            },
            '&:hover fieldset': {
                borderColor: 'yellow',
            },
            '&.Mui-focused fieldset': {
                borderColor: 'green',
            },
        },
    },
})(TextField);
const ranges = [
    {
        value: '0-20',
        label: '0 to 20',
    },
    {
        value: '21-50',
        label: '21 to 50',
    },
    {
        value: '51-100',
        label: '51 to 100',
    },
];

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',

    },
    margin: {
        margin: theme.spacing(1),
    },
    withoutLabel: {
        marginTop: theme.spacing(3),
    },
    textField: {
        flexBasis: 200,
    },
}));
function NumberFormatCustom(props) {
    const { inputRef, onChange, ...other } = props;

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={values => {
                onChange({
                    target: {
                        value: values.value,
                    },
                });
            }}
            thousandSeparator
            isNumericString
            prefix=""
        />
    );
}
const classes = useStyles


export default class InputAdornments extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cant: "",
            emp: "",
            tEn: "",
            arrClave: [],
            depa: "",
            listDepa: [],
            nom: ""
        }
    }
    shuffle(arr) {
        var i,
            j,
            temp;
        for (i = arr.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        var resArr = arr[1] + arr[2] + arr[3]
        return resArr;
    };
    Generar() {
        var cantidad = this.state.cant
        var idEmp = this.state.emp
        empresaID = this.state.emp
        nombreE = this.state.nom
        cantEn = this.state.cant
        arrMiClave = []

        if (cantidad < 50) {
            tipoEncuesta = "A"
        } else {
            tipoEncuesta = "B"
        }
        for (var i = 0; i < cantidad; i++) {
            arrMostrar[i] = this.shuffle(arrTodo)
            arrMiClave[i] = idEmp + tipoEncuesta + arrMostrar[i] + "\n"
        }
        this.setState({
            arrClave: arrMiClave,
            tEn: tipoEncuesta
        })

    }

    render() {
        return (
            <section style={{ marginTop: '50px', marginLeft: '20px' }}>
                <section className={classes.root} style={{ marginTop: '100px' }}>
                    <TextField
                        id="id-email"
                        label="Email"
                        className={classes.root}
                        margin="Email"
                        type="email"

                    />
                    <TextField
                        id="id-contra"
                        label="Password"
                        className={classes.root}
                        margin="Password"
                        type="password"
                    />


                    <Button
                        style={{ marginRight: '20px', marginLeft: '20px' }}
                        className={classes.root}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        onClick={() => {
                            var email = document.getElementById('id-email').value
                            var password = document.getElementById('id-contra').value
                            firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
                                // Handle Errors here.
                                var errorCode = error.code;
                                var errorMessage = error.message;
                                alert("Error " + errorMessage + " " + errorCode)
                                // ...
                            });
                        }}
                    >Crear cuenta</Button>
                </section>
                <FormControl className={classes.margin} style={{ marginRight: '20px', width: '500px' }} >
                    <CssTextField className={classes.margin} id="id-Nombre-Empresa" label="Nombre de la empresa"
                        onChange={e => this.setState({ nom: e.target.value })}
                        value={this.state.nom} />
                </FormControl>
                <div className={classes.root}>

                    <FormControl className={classes.margin} style={{ marginTop: '30px', marginRight: '20px', marginLeft: '20px' }}>
                        <CssTextField
                            className={classes.margin}
                            label="ID empresa"
                            variant="outlined"
                            id="id-Empresa"
                            onChange={e => this.setState({ emp: e.target.value })}
                            value={this.state.emp}
                        />

                    </FormControl>
                    <FormControl className={classes.margin} style={{ marginTop: '30px', marginLeft: '20px' }}>
                        <CssTextField
                            className={classes.margin}
                            label="Cantidad de encuestas"
                            variant="outlined"
                            id="id-cantidad"
                            InputProps={{
                                inputComponent: NumberFormatCustom,
                            }}
                            onChange={e => this.setState({ cant: e.target.value })}
                            value={this.state.cant}
                        />
                    </FormControl>
                </div>

                {
                    //Botones departamentos
                }
                <section className={classes.root} style={{ marginTop: '30px' }}>
                    <TextField
                        id="id-depa"
                        label="Departamento"
                        className={classes.root}
                        margin="Departamento"
                        value={this.state.depa}
                        onChange={e => this.setState({ depa: e.target.value })}
                    />


                    <Button
                        style={{ marginRight: '20px', marginLeft: '20px' }}
                        className={classes.root}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        onClick={() => {
                            arrDepaFB.push(this.state.depa)
                            arrDepaFBPro.push(this.state.depa + ",")
                            this.setState({

                                // listDepa: [this.state.listDepa,<hr/>,this.state.depa]
                                listDepa: arrDepaFBPro
                            })
                        }}
                    >Agregar</Button>
                    <Button
                        className={classes.root}
                        variant="contained"
                        color="secondary"
                        className={classes.button}
                        onClick={() => {
                            this.setState({
                                listDepa: []
                            })
                        }}
                    >Eliminar</Button>
                    <List className={classes.root} subheader={<li />}>
                        <p>{this.state.listDepa}</p>
                    </List>
                </section>
                {
                    //Boton generar claves
                }

                <section className={classes.root}>
                    <Button
                        className={classes.root}
                        variant="contained"
                        color="primary"
                        className={classes.button}
                        onClick={e => (this.Generar())}
                    >Generar Claves</Button>
                    <List className={classes.root} subheader={<li />}>
                        <p>{this.state.arrClave}
                            {//this.state.listClave
                            }</p>
                    </List>
                </section>
                {
                    //Boton subir a Firebase
                }
                <Button
                    className={classes.root}
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    onClick={e => (subirFirebase())}
                >Subir</Button>
            </section>
        );
    }
}

function subirFirebase() {
    for (var i = 0; i < arrMostrar.length; i++) {
        firebase.database().ref('empresa/' + empresaID + '/' + tipoEncuesta + '/' + 'clave/' + arrMostrar[i]).update({
            //sumar valor
            valor: "1"
        })
    }
    firebase.database().ref('empresa/' + empresaID + '/' + tipoEncuesta).update({
        //sumar valor
        cont: 0,
        cant: cantEn
    })
    for (var i = 0; i < arrDepaFB.length; i++) {
        firebase.database().ref('empresa/' + empresaID + '/' + tipoEncuesta + '/depa/' + arrDepaFB[i]).update({
            //sumar valor
            cont: 0,
            final: 0,
            ambiente: 0 ,
            //Dominio
            condiciones: 0 ,
            //Categoria Factores
            factores: 0 ,
            //Dominio
            carga: 0 ,
            falta: 0 ,
            //Categoria Org
            org: 0 ,
            //Dominio
            jornada: 0 ,
            inter: 0 ,
            //Categoria Liderazgo
            liderCat: 0 ,
            //Dominio
            lider: 0 ,
            relaciones: 0 ,
            violencia: 0 ,
            //Categoria Entorno
            entorno: 0 ,
            //Dominio
            reconocimiento: 0 ,
            insuficiente: 0 ,
            di1: 0 ,
            di2: 0 ,
            di3: 0 ,
            di4: 0 ,
            di5: 0 ,
            di6: 0 ,
            di7: 0 ,
            di8: 0 ,
            di9: 0 ,
            di10: 0 ,
            di11: 0 ,
            di12: 0 ,
            di13: 0 ,
            di14: 0 ,
            di15: 0 ,
            di16: 0 ,
            di17: 0 ,
            di18: 0 ,
            di19: 0 ,
            di20: 0 ,
            di21: 0 ,
            di22: 0 ,
            di23: 0 ,
            di24: 0 ,
            di25: 0 

        })
    }
    firebase.database().ref('empresa/' + empresaID + '/' + tipoEncuesta + '/general/').update({
        //sumar valor
        cont: 0,
        final: 0,
        ambiente: 0 ,
        //Dominio
        condiciones: 0 ,
        //Categoria Factores
        factores: 0 ,
        //Dominio
        carga: 0 ,
        falta: 0 ,
        //Categoria Org
        org: 0 ,
        //Dominio
        jornada: 0 ,
        inter: 0 ,
        //Categoria Liderazgo
        liderCat: 0 ,
        //Dominio
        lider: 0 ,
        relaciones: 0 ,
        violencia: 0 ,
        //Categoria Entorno
        entorno: 0 ,
        //Dominio
        reconocimiento: 0 ,
        insuficiente: 0 ,
        di1: 0 ,
        di2: 0 ,
        di3: 0 ,
        di4: 0 ,
        di5: 0 ,
        di6: 0 ,
        di7: 0 ,
        di8: 0 ,
        di9: 0 ,
        di10: 0 ,
        di11: 0 ,
        di12: 0 ,
        di13: 0 ,
        di14: 0 ,
        di15: 0 ,
        di16: 0 ,
        di17: 0 ,
        di18: 0 ,
        di19: 0 ,
        di20: 0 ,
        di21: 0 ,
        di22: 0 ,
        di23: 0 ,
        di24: 0 ,
        di25: 0 

    })

    for (var i = 0; i < arrDepaFB.length; i++) {
        firebase.database().ref('cliente/id/' + empresaID + '/depa/' + i).update({
            //sumar valor
            nombre: arrDepaFB[i]
        })
    }
    alert(nombreE)
    firebase.database().ref('cliente/id/' + empresaID).update({
        //sumar valor
        nombre: nombreE,
        cant: cantEn,
        id: empresaID,
        tipo: tipoEncuesta,
        claves: arrMostrar,
        cantDepa: arrDepaFB.length
    })



}