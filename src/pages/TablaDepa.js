import React from 'react';
import Table from '../components/tabla';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';

import Button from '@material-ui/core/Button';
import { redA700, orange500, yellow500, lightGreenA400, cyanA400 } from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import { GraficaBarra } from '../components/graficas/GraficaBarra';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import GraficaCaliFin from '../components/GraficaCaliFIn';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});
var mOpenMA = false, mOpenA = false, mOpenM = false, mOpenB = false, mOpenN = false

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://jefteapps.com/">
                JeftéApps
        </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const drawerWidth = 240;

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    toolbar: {
        paddingRight: 24, // keep right padding when drawer closed
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar,
    },
    appBar: {
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    menuButtonHidden: {
        display: 'none',
    },
    title: {
        flexGrow: 1,
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9),
        },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
        flexGrow: 1,
        height: '100vh',
        overflow: 'auto',
    },
    container: {
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
    },
    paper: {
        padding: theme.spacing(2),
        display: 'flex',
        overflow: 'auto',
        flexDirection: 'column',
    },
    fixedHeight: {
        height: 240,
    },
}));
export default class TablaDepartamento extends React.Component {

    constructor(props) {
        super(props);
        this.setState({
            lugar: 2,
            empresa: this.props.idEmpresa,
            encuesta: this.props.idEncuesta,
            depa: this.props.depa,
            open: false,
            setOpen: false,
            titulo: "",
            texto: ""
        })
    }
    variables(valor) {
        switch (valor) {
            case 1:
                const { idLugar } = this.props.location.state
                return (idLugar)
                break
            case 2: const { idEmpresa } = this.props.location.state
                return (idEmpresa)
                break
            case 3: const { idEncuesta } = this.props.location.state
                return (idEncuesta)
                break
            case 4: const { idDepa } = this.props.location.state
                return (idDepa)
                break
        }
    }
    handleClickOpenMA = () => {
        this.setState({ open: true })
        mOpenMA = true
    };
    handleClickOpenA = () => {
        this.setState({ open: true })
        mOpenA = true
    };
    handleClickOpenM = () => {
        this.setState({ open: true })
        mOpenM = true
    };
    handleClickOpenB = () => {
        this.setState({ open: true })
        mOpenB = true
    };
    handleClickOpenN = () => {
        this.setState({ open: true })
        mOpenN = true
    };


    handleClose = () => {
        this.setState({ open: false })
        mOpenMA = false
        mOpenA = false
        mOpenM = false
        mOpenB = false
        mOpenN = false
    };
    nivelRiesgo = (lvl) => {
        switch (lvl) {
            case 0:
                mOpenN = true
                break
            //e => this.setState({ setFolio: e.target.value })
            case 1:
                mOpenB = true
                break
            case 2:
                mOpenM = true
                break
            case 3:
                mOpenA = true

                break
            case 4:
                mOpenMA = true
                break
        }

    }

    render() {
        return (
            <div>
                <Link to={{ pathname: '/' }}
                    style={{ textDecoration: 'none', color: 'black' }}>
                     <RaisedButton style={{ marginTop: '30px', marginLeft: '30px' }}
                        label="Inicio"
                        //secondary={true}
                    />
                    <RaisedButton style={{ marginTop: '30px', marginLeft: '5px' }}
                        label="Cerrar sesión"
                        //primary={true}
                        onClick={e => (sessionStorage.clear())}
                    />
                    <RaisedButton style={{ marginTop: '30px', marginLeft: '5px' }}
                        label="imprimir"
                        //primary={true}
                        onClick={e => (window.print())}
                    />
                    
                </Link>
                <Container maxWidth="lg">
                    <h3>Dashboard Departamento de {this.variables(4)}</h3>
                    <Grid container spacing={2}>
                        {/* Chart */}
                        <Grid item xs={12} md={8} lg={4} container={true}>
                            <Paper >
                                <h4>Nivel de riesgo por color</h4>
                                <Button onClick={() => this.nivelRiesgo(4), this.handleClickOpenMA} style={{ 'backgroundColor': redA700, 'marginLeft': '10px' }}>Muy Alto</Button>
                                <Dialog
                                    open={mOpenMA}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">{"Nivel de riesgo MUY ALTO"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Se requiere realizar el análisis de cada categoría y dominio para establecer las acciones de intervención apropiadas, mediante un Programa de intervención que deberá incluir evaluaciones específicas1, y contemplar campañas de sensibilización, revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión.
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose} color="primary">
                                            Ok
                        </Button>
                                    </DialogActions>
                                </Dialog>
                                <Button onClick={() => this.nivelRiesgo(3), this.handleClickOpenA} style={{ 'backgroundColor': orange500 }}>Alto</Button>
                                <Dialog
                                    open={mOpenA}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">{"Nivel de riesgo ALTO"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Se requiere realizar un análisis de cada categoría y dominio, de manera que se puedan determinar las acciones de intervención apropiadas a través de un Programa de intervención, que podrá incluir una evaluación específica1 y deberá incluir una campaña de sensibilización, revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión.
                            </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose} color="primary">
                                            Ok
                            </Button>
                                    </DialogActions>
                                </Dialog>
                                <Button onClick={() => this.nivelRiesgo(2), this.handleClickOpenM} style={{ 'backgroundColor': yellow500 }}>Medio</Button>
                                <Dialog
                                    open={mOpenM}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">{"Nivel de riesgo MEDIO"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Se requiere revisar la política de prevención de riesgos psicosociales y programas para la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral, así como reforzar su aplicación y difusión, mediante un Programa de intervención.
                            </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose} color="primary">
                                            Ok
                            </Button>
                                    </DialogActions>
                                </Dialog>
                                <Button onClick={() => this.nivelRiesgo(1), this.handleClickOpenB} style={{ 'backgroundColor': lightGreenA400 }}>Bajo</Button>
                                <Dialog
                                    open={mOpenB}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">{"Nivel de riesgo BAJO"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            Es necesario una mayor difusión de la política de prevención de riesgos psicosociales y programas para: la prevención de los factores de riesgo psicosocial, la promoción de un entorno organizacional favorable y la prevención de la violencia laboral.
                            </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose} color="primary">
                                            Ok
                            </Button>
                                    </DialogActions>
                                </Dialog>
                                <Button onClick={() => this.nivelRiesgo(0), this.handleClickOpenN} style={{ 'backgroundColor': cyanA400, 'marginRight': '10px' }}>Nulo</Button>
                                <Dialog
                                    open={mOpenN}
                                    TransitionComponent={Transition}
                                    keepMounted
                                    onClose={this.handleClose}
                                    aria-labelledby="alert-dialog-slide-title"
                                    aria-describedby="alert-dialog-slide-description"
                                >
                                    <DialogTitle id="alert-dialog-slide-title">{"Nivel de riesgo NULO"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-slide-description">
                                            El riesgo resulta despreciable por lo que no se requiere medidas adicionales.
                            </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={this.handleClose} color="primary">
                                            Ok
                            </Button>
                                    </DialogActions>
                                </Dialog>
                            </Paper>
                        </Grid>
                        {/* Recent Deposits */}
                        <Grid item xs={12} md={4} lg={4} alignContent='center' container={true}>
                            <Paper >
                                <h4>Porcentaje final</h4>
                                <GraficaBarra idLugar={this.variables(1)}
                                    idEmpresa={this.variables(2)}
                                    idEncuesta={this.variables(3)}
                                    idDepa={this.variables(4)}>

                                </GraficaBarra>

                            </Paper>
                        </Grid>

                        <Grid item xs={12} md={2} lg={4} alignContent='center' container={true}>
                            <Paper >
                                <GraficaCaliFin idLugar={this.variables(1)}
                                    idEmpresa={this.variables(2)}
                                    idEncuesta={this.variables(3)}
                                    idDepa={this.variables(4)}
                                ></GraficaCaliFin>


                            </Paper>
                        </Grid>

                        {/* Recent Orders */}
                        <Grid item xs={12}>
                            <Paper>
                                <Table
                                    idLugar={this.variables(1)}
                                    idEmpresa={this.variables(2)}
                                    idEncuesta={this.variables(3)}
                                    idDepa={this.variables(4)}
                                ></Table>
                            </Paper>
                        </Grid>
                    </Grid>
                </Container>
                <Copyright />
            </div>

            /*

                <Grid item xs={12}>

                    <Paper style={{
                        padding: '2px',
                        display: 'flex',
                        overflow: 'auto',
                        flexDirection: 'column'
                    }}>{

                            // alert("mi valor " + this.variables(1))
                        }
                        <Table
                            idLugar={this.variables(1)}
                            idEmpresa={this.variables(2)}
                            idEncuesta={this.variables(3)}
                            idDepa={this.variables(4)}
                        ></Table>
                    </Paper>
                </Grid>
                {//alert(this.state.sAmbiente)
                }
            </div>
            */
        );
    }
}
